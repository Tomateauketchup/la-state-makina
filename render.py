import cmarkgfm

file = open("README.md", 'r')
markdown_text = file.read()
file.close()

html = cmarkgfm.markdown_to_html(markdown_text)
res = open("README.html", 'w')
res.write(html)
res.close()


