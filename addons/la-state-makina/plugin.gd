@tool
class_name StateMachineEditorPlugin
extends EditorPlugin

static var state_machine_editor:StateMachineEditorEmbedded
var focused_object
var tool_dock

func _enter_tree():
	# Initialization of the plugin goes here.
	# Add the new type with a name, a parent type, a script and an icon.	
	#add_custom_type("SMBaseNode", "Node", preload("src/editor/state_machine/sm_base_node.gd"), preload("src/editor/state_machine/sm_base_node_icon.svg"))
	#add_custom_type("SMState", "SMBaseNode", preload("src/editor/data/sm_state_data.gd"), preload("src/editor/data/sm_state_data_icon.svg"))
	add_custom_type("SMEStateData", "Node", preload("src/editor/data/sme_state_data.gd"), preload("src/editor/data/sme_state_data_icon.svg"))
	add_custom_type("SMETransitionData", "Node", preload("src/editor/data/sme_transition_data.gd"), preload("src/editor/data/sme_transition_data_icon.svg"))
	add_custom_type("SMESpec", "Node", preload("src/editor/data/sme_spec.gd"), preload("src/editor/data/sme_spec_icon.svg"))
	add_custom_type("SMEEditor", "GraphEdit", preload("src/editor/ui/state_machine_editor.gd"), preload("src/editor/ui/state_machine_editor_icon.svg"))
	if not state_machine_editor:
		state_machine_editor=StateMachineEditorEmbedded.new()
		state_machine_editor.connect("update_json_state_machine", _on_update_json_state_machine)
	state_machine_editor.undo_redo_manager = get_undo_redo()
	state_machine_editor.editor_interface = get_editor_interface()
	add_control_to_bottom_panel(state_machine_editor, "State Machine Viewer")			
	print("Loading StateMachine Plugin success")

	tool_dock = preload("res://addons/la-state-makina/src/editor/ui/state_machine_dock.tscn").instantiate()
	tool_dock.register_editor_plugin(self)
	add_control_to_dock(EditorPlugin.DOCK_SLOT_LEFT_UR, tool_dock)
	
func _exit_tree():	
	#remove_custom_type("SMSpec")
	#remove_custom_type("SMState")
	remove_control_from_bottom_panel(state_machine_editor)
	remove_custom_type("SMEStateData")
	remove_custom_type("SMETransitionData")
	remove_custom_type("SMESpec")
	remove_custom_type("SMEEditor")
	
	remove_control_from_docks(tool_dock)
	tool_dock.free() # Erase the control from the memory
	
	#state_machine_editor.dispose()
	state_machine_editor=null
	focused_object=null	
	print("Closed StateMachine Plugin success")
	
func _handles(object):			
	if object is SMESpec:
		return true	
	elif object is SMEStateData:
		return true
	elif object is SMETransitionData:
		return true
	return false

func _edit(object):
	set_focused_object(object)

func set_focused_object(obj):
	if focused_object != obj:
		focused_object = obj
		_on_focused_object_changed(obj)

func find_sm_spec(obj)->SMESpec:
	if obj is SMESpec:
		return obj
	if (obj is SMEStateData or obj is SMETransitionData) and obj.get_parent() is SMESpec:
		return obj.get_parent()
	return null

func _on_focused_object_changed(obj) :	
	var hide=true
	if focused_object:
		var sm_spec=find_sm_spec(focused_object)
		if sm_spec != null:
			hide=false	
			state_machine_editor.set_sme_spec_data(sm_spec)
			show_state_machine_editor()	
	if hide : 
		hide_state_machine_editor()
func show_state_machine_editor():	
	if focused_object and state_machine_editor:		
		if state_machine_editor.is_inside_tree():								
			make_bottom_panel_item_visible(state_machine_editor)
		
		

func restore_scroll():
	state_machine_editor.restore_scroll()

func hide_state_machine_editor():		
	if state_machine_editor and state_machine_editor.is_inside_tree():
		state_machine_editor.set_sme_spec_data(null)
		#remove_control_from_bottom_panel(state_machine_editor)

func _on_update_json_state_machine(json_path:String):
	var files : PackedStringArray = PackedStringArray([json_path])
	push_warning("TODO : Solve the Reimport BUG in la state makina")
	EditorInterface.get_resource_filesystem().reimport_files(files)
