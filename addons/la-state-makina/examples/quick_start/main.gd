extends Node

var sm : SMStateMachine
var sm_api_ressource = preload("./music_player_api_impl.gd")
var sm_api

var sm_scene : PackedScene = preload("./music_player.tscn")
var sm_viewer : StateMachineEditor

func _init():
	# Create the state machine
	sm = SMStateMachine.new()
	sm.load_json("res://addons/la-state-makina/examples/quick_start/music_player.json")
	sm_api = sm_api_ressource.new()
	sm.register_api(sm_api)

	# Create the state machine viewer
	sm_viewer=StateMachineEditor.new()
	sm_viewer.show_edit_button=false
	sm_viewer.size = DisplayServer.window_get_size() 
	var force_redraw = true
	var sm_name : String = "Music Player"
	sm_viewer.set_data(
		sm_scene, sm_scene.instantiate(), sm, sm_name, force_redraw
	)
	add_child(sm_viewer)

func _ready():
	sm.start()

func _process(_delta: float):
	sm.update()
	sm_viewer.show_active_states(sm.actives_states_to_uuid_states())
