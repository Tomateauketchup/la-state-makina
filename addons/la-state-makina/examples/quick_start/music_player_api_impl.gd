extends "./music_player_api.gd"

var title = ""
var init_time_limit = 3 # in seconds

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_init_music_player(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	print("Try to init the music player.")
	title = "The Great JukeBox"
	return stop_update

## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_display_end_of_intitialization(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	print("The music player \"%s\" is ready ."%title)
	return stop_update

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_initization_is_finished(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return false

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_an_emergency_occurs(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	if (
		_sm.is_active("WAITING_END_OF_INITIALIZATION") && 
		( 
			_sm.get_time() > _sm.get_last_activation_time("WAITING_END_OF_INITIALIZATION") +
			init_time_limit 
		)
	):
		return true
	return false

## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_emergency_stop(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	print("Activation of the emmergency mode !")
	return stop_update
