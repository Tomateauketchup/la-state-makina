## This file is the API of the state machine defined in the following Json File :[br]
## res://addons/la-state-makina/examples/simple_example/simple_example.json[br][br]
##
## This file was automatically generated by the script
## res://addons/la-state-makina/src/state_machine/sm_state_machine_data.gd[br][br]
##
## Read the following tutorial for more detail how to use Machine State API : [br]
## @tutorial: res://addons/la-state-makina/README.md
## @tutorial: https://gitlab.com/Tomateauketchup/la-state-makina

#
# Don't modify that script. Extend it instead.
#

# If you need to change the parent class of that generated API
# add or modify in the JSON description of you state machine the following
# field :
# "extends_class": "THE_NEW_PARENT_CLASS"
extends CenterContainer


#
# API for activation, functions of some states
#

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_init_connection(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_init_connection is Not Implemented')
	return stop_update

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_load_ui(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_load_ui is Not Implemented')
	return stop_update

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_set_on(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_set_on is Not Implemented')
	return stop_update

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_set_off(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_set_off is Not Implemented')
	return stop_update


#
# API for deactivation, functions of some states
#

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_close_connection(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_close_connection is Not Implemented')
	return stop_update


#
# API for in, functions of some states
#

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_display_error(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_display_error is Not Implemented')
	return stop_update

## This is the implementation of a function associated with a state. 
## This function is called during a micro_update when some
## transitions is crossed. All those transitions are incident to the state.[br][br]
## 
## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_emergency(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_emergency is Not Implemented')
	return stop_update


#
# API for Micro Update functions of some states
#


#
# API for Updates functions of some states
#

## This is the implementation of a function associated with a state. 
## At the end of an update, if a state is always active, this function is run.
## Be carefull this function is never called during a Micro Update.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _state_id] is the state id of the update function.[br]
## [param _delay] is the time in seconds since the last state machine update.
func ef_display_progress(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	push_warning('Funcion ef_display_progress is Not Implemented')
	return stop_update


#
# API for event functions used to test if an event have to be raised.
#

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_data_is_finished(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_data_is_finished is Not Implemented')
	return false

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_ready(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_is_ready is Not Implemented')
	return false

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_an_error_occurs(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_an_error_occurs is Not Implemented')
	return false

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_on(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_is_on is Not Implemented')
	return false

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_off(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_is_off is Not Implemented')
	return false

## That function is associated with some events.
## To cross an or/and transition, it's event have to be raised.
## When an/all origin/s of thoses transitions are actives, this function is executed
## to determine if the transtion should be crossed. 
## On a true result, the event associated with that function is raised and the transition is crossed.
## On false, nothing happen.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_ui_is_load(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	push_warning('Funcion ef_ui_is_load is Not Implemented')
	return false


#
# API for transition functions
#

## This is the implementation of a function associated with some transitions. 
## Each time one of those transitions is crossed, that function is run.[br][br]
## 
## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_emit_starting(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	push_warning('Funcion ef_emit_starting is Not Implemented')
	return stop_update

