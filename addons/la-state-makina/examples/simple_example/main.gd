extends Node

var sm : SMStateMachine
var sm_api_ressource = preload("./simple_example_api_impl.gd")
var sm_api : Control

var sm_scene : PackedScene = preload("./simple_example.tscn")
var sm_viewer : StateMachineEditor
var v_box_container = VBoxContainer

func _init():	
	# Create the state machine
	sm = SMStateMachine.new()
	sm.load_json("res://addons/la-state-makina/examples/simple_example/simple_example.json")
	sm_api = sm_api_ressource.new()
	sm.register_api(sm_api)
	sm_api.custom_minimum_size.y=150
	
	# Create the state machine viewer
	sm_viewer=StateMachineEditor.new()
	sm_viewer.show_edit_button=false
	var force_redraw = true
	var sm_name : String = "Simple Example"
	sm_viewer.set_data(
		sm_scene, sm_scene.instantiate(), sm, sm_name, force_redraw
	)
	sm_viewer.size_flags_horizontal += Control.SIZE_EXPAND
	sm_viewer.size_flags_vertical += Control.SIZE_EXPAND
	
	v_box_container = VSplitContainer.new()
	v_box_container.add_child(sm_api)
	v_box_container.add_child(sm_viewer)
	add_child(v_box_container)
	v_box_container.anchors_preset = VSplitContainer.PRESET_FULL_RECT	

func _ready():
	sm.start()

func _process(_delta: float):
	sm.update()
	sm_viewer.show_active_states(sm.actives_states_to_uuid_states())
