extends "res://addons/la-state-makina/examples/simple_example/simple_example_api.gd"

var is_on : bool
var is_ready : bool
var h_box : HBoxContainer
var on_off_button : Button
var user_ready_button : Button
var display : Label
var download_time = 6
var error_time = 10

func _init():
	is_on = false
	is_ready = false
	h_box = HBoxContainer.new()

	on_off_button = Button.new()
	on_off_button.pressed.connect(click)
	on_off_button.text = "on"

	display = Label.new()
	display.custom_minimum_size.x = 300
	display.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
	display.vertical_alignment = VERTICAL_ALIGNMENT_CENTER
	
	user_ready_button = Button.new()
	user_ready_button.pressed.connect(ready)
	user_ready_button.text = "Start"
	
	on_off_button.hide()
	user_ready_button.hide()
	display.hide()
	
	h_box.add_child(user_ready_button)
	h_box.add_child(on_off_button)
	h_box.add_child(display)
	add_child(h_box)

func click():
	is_on =not(is_on)

func ready():
	is_ready = true

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_init_connection(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	print("Simple example : Init connection !")
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_load_ui(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	on_off_button.hide()
	user_ready_button.hide()
	display.show()
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_set_on(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	display.text = "ON :)"
	on_off_button.text = "TURN OFF"
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_set_off(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	display.text = "OFF :("
	on_off_button.text = "TURN ON"
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_close_connection(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	print("Simple example : Close connection !")
	display.text = "Download is finished"
	user_ready_button.show()
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_display_error(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	var text = "Error occurs : your are too slow !"
	display.text = text
	print("Simple example : " + text)
	return stop_update

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_emergency(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	on_off_button.hide()
	user_ready_button.hide()
	display.show()
	return stop_update

## [param _sm] is the machine state.[br]
## [param _state_id] is the state id of the update function.[br]
## [param _delay] is the time in seconds since the last state machine update.
func ef_display_progress(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	var elapsed_time = _sm.get_time() - _sm.get_last_activation_time("DOWNLOAD_DATA")
	var progress : int = int(elapsed_time/download_time * 100)
	display.text = str(progress) + "%"
	return stop_update

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_data_is_finished(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return _sm.get_time() > _sm.get_last_activation_time("DOWNLOAD_DATA") + download_time

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_ready(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return is_ready

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_an_error_occurs(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return _sm.get_time() > _sm.get_last_activation_time("DOWNLOAD_DATA") + error_time

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_on(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return is_on

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_is_off(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return not is_on

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_ui_is_load(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	return true

## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_emit_starting(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	on_off_button.show()
	user_ready_button.hide()
	return stop_update

## External function that is executed at the end of the state machine 
## [SMStateMachine.start].
##
## [param _sm] : The state machine that execute the external function
func ef_post_start(_sm:SMStateMachine):
	pass

## External function that is executed at the begining of the state machine 
## [SMStateMachine.update].
##
## [param _sm] : The state machine that execute the external function.
## [param _delta] : The delay since the begining of the last update.
func ef_pre_update(_sm:SMStateMachine, _delta:float):
	pass

## External function that is executed at the end of the state machine 
## [SMStateMachine.update].
##
## [param _sm] : The state machine that execute the external function.
## [param _delta] : The delay since the begining of the last update.
func ef_post_update(_sm:SMStateMachine, _delta:float):
	pass
