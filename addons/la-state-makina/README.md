'La-state-makina' is an asset for the game engine Godot.

This asset is written in gdscript and implements a machine state that follows
the evolution rules of a Grafcet, a particular norm of automaton.

See, the [wikipedia page on grafcet](https://fr.wikipedia.org/wiki/Grafcet) for
more details.

----

[[_TOC_]]

----

# Installation

Copy the following folder 
```plaintext
addons/la-state-makina
```
in your addons folder of your godot project.

Open the Godot Editor, ane go to : 
```plaintext
Project > Project settings ... > Plugins 
```

Then, enable the plugin `la-state-makina`.

# Quick start

To implement a new state machine, you need to follow a special workflow.

First, we present briefly the workflow. Then, we explain in details, each steps
of the workflow.

The forkflow is the following : 
1) Define you machine state in a file named `YOUR_STATAE_MACHINE.json` written
   in JSON format.
2) In the Godot editor, use the `la-state-makina` tools to automatically
   generate two files, `YOUR_STATE_MACHINE_api.gd` and
   `YOUR_STATE_MACHINE.tscn`: 
  - the file `YOUR_STATE_MACHINE_api.gd` is an API, written in 
    GdScript. This API contains all the methods called by the state machine 
    during its execution,
  - the file `YOUR_STATE_MACHINE.tscn` is a Godot scene that allows
    displaying the state machine inside the Godot Editor or within the game.
3) Edit, in the Godot editor, the node and edge positions of the machine state 
   in the generated scene : `YOUR_STATE_MACHINE.tscn`.
4) Implement, in a new file named `YOUR_STATE_MACHINE_api_impl.tscn` all the
   functions defined by the generated API present in 
   `YOUR_STATE_MACHINE_api.gd`.
5) Instanctiate and update your state machine in your application.
6) Optionally, displaying the execution of the state machine in game.

Now, let's describe these steps by creating a state machine for a music player,
as shown in the following image:"

![Image of a machine state for a music player](doc/images/music_player_state_machine.png "A machine state for a music player")

This example can be consulted in `addons/la-state-makina/examples/quick_start`.

## 1) Writting a machine state description in Json

The preceding image illustrates a state machine comprising states and
transitions. Specifically:
 - States are depicted as green boxes, with the state's name written in the
   box's title. Activation, deactivation, and update functions are declared
   within each state's box. States with a pink border are the initial states of
   the state machine.
 - Transitions are represented by arrows with brown boxes. The title of these 
   boxes are the name of the event raised when one of the `evt_fcts` returns
   true.
   Those transitions are crossed when their events are raised.
   Upon traversing a transition, the functions declared in `fcts` are executed.

To define all the states and transitions of the music player state machine,
create the file `music_player.json` containing all its description with the 
following format : 
```plaintext
{
   "description": "This is an example of machine states to manage music player.",
   "events": [
      {
         "fcts": "ef_initization_is_finished",
         "name": "INITIALIZATION_IS_FINISHED"
      },
      {
         "fcts": "ef_user_click_play_button",
         "name": "USER_ASK_TO_PLAY"
      },
      {
         "fcts": "ef_user_click_stop_button",
         "name": "USER_ASK_TO_STOP"
      },
      {
         "fcts": "ef_an_emergency_occurs",
         "name": "EMERGENCY_OCCURS"
      },
      {
         "fcts": "ef_ask_to_restart",
         "name": "USER_ASK_TO_RESTART"
      }
   ],
   "init_states": [
      "WAITING_END_OF_INITIALIZATION"
   ],
   "name": "music_player",
   "states": [
      {
         "fcts_activation": "ef_init_music_player",
         "name": "WAITING_END_OF_INITIALIZATION"
      },
      {
         "fcts_activation": "ef_start_music",
         "fcts_deactivation": "ef_stop_music",
         "fcts_update": "ef_play_music",
         "name": "MUSIC_IS_PLAYING"
      },
      {
         "name": "WAITING_A_PLAY_ORDER"
      },
      {
         "name": "EMERGENCY_STOP"
      }
   ],
   "transitions": [
      {
         "ends": "EMERGENCY_STOP",
         "event": "EMERGENCY_OCCURS",
         "fcts": "ef_emergency_stop",
         "origins": "*",
         "type": "or"
      },
      {
         "ends": "WAITING_A_PLAY_ORDER",
         "event": "INITIALIZATION_IS_FINISHED",
         "fcts": "ef_display_end_of_intitialization",
         "origins": "WAITING_END_OF_INITIALIZATION"
      },
      {
         "ends": "MUSIC_IS_PLAYING",
         "event": "USER_ASK_TO_PLAY",
         "origins": "WAITING_A_PLAY_ORDER"
      },
      {
         "ends": "WAITING_A_PLAY_ORDER",
         "event": "USER_ASK_TO_STOP",
         "origins": "MUSIC_IS_PLAYING"
      },
      {
         "ends": "WAITING_END_OF_INITIALIZATION",
         "event": "USER_ASK_TO_RESTART",
         "origins": [
            "MUSIC_IS_PLAYING",
            "WAITING_A_PLAY_ORDER"
         ],
         "fcts": "ef_close_music_player",
         "type": "or"
      }
   ],
   "version": "0.1"
}
```

## 2) Generating an API script and a scene for you state machine

Now you can use the `la-state-makina` plugin to generate an API file and a scene
file for your state machine.

In the `dock` window, click on the `SM` tab. Then click on the
`Generate Scene and API from json` button, and select the file 
`music_player.json`.
The following image shows the editor tab used to generate API and scene.
This tab is located at the top and left of the image.

![Image of the editor tab used to generate API end scene.](doc/images/generate_api_and_scene.png "Generating api and scene")

Normally, two files has been created : `music_player_api.gd` and 
`music_player.tscn`. This can be observed in the filesystem dock, as depicted in
the following image :

![The file system dock with new files.](doc/images/file_system.png "The filesystem dock")


The file `music_player_tscn` allows you to edit and display graphically your 
state machine.

The file `music_player_api.json` defines all the functions you need to implement.


## 3) Editing the scene of your state machine

In the godot Editor, open the scene `music_player.tscn` and click on the root 
node of the scene to update the scene file.
Now you can open the `state machine viewer` by clicking on the corresponding 
tab present in the bottom panel.

You will obtain the following result :

![An image of the viewer of the state machine.](doc/images/state_machine_viewer.png "The viewer of the state machine.")

All nodes and transistions are set at the same position, and an unique UUID
is automatically generated for each state and transition.

Due to a bug, sometimes the generated files are not correctly reimported.
In such cases, you need to close and reopen all the state machine files 
(JSON and GdScript).

Now, you can move the nodes and transitions by clicking on the `Edit button` 
inside the `State Machine viewer` to achive the well-organized state machine
presented at the beginning of the document.

To break the curves of the arcs, double-click on the specific arcs where you
want to introduce a break. You can then move these newly created nodes.

## 4) Implementing the api of your state machine

Now you need to implement the state machine API in a specific script.

Simply create a script that extends the `music_player_api.gd` script. 

For the music player state machine, use the following name for that script : 
`music_player_api_impl.gd`.

An empty implementation is given by : 
```plaintext
extends "./music_player_api.gd"
```

In that file, you can now implement all the functions defined in 
`music_player_api.gd`. 

For example, in the following lines, we offer a flawed implementation of the 
music player state machine by implementing the faulty function 
"an_emergency_occurs" that returns false during initialization.
In this function, we detect that initialization has not completed by measuring 
the initialization time.
After 3 seconds, we transition the machine to the emergency state.

```plaintext
extends "./music_player_api.gd"

var title = ""
var init_time_limit = 3 # in seconds

## [param _sm] is the machine state[br]
## [param _state_id] is the state id incident of the crossed transitions.[br]
## [param _trans_ids] is the array of all transitions ids that had been crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
## (To obtain the update counter, use the machine state [param _sm])
func ef_init_music_player(_sm: SMStateMachine, _state_id:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
   var stop_update = false
   print("Try to init the music player.")
   title = "The Great JukeBox"
   return stop_update

## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_display_end_of_intitialization(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
   var stop_update = false
   print("The music player \"%s\" is ready ."%title)
   return stop_update

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_initization_is_finished(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
   return false

## [param _sm] is the machine state.[br]
## [param _event_id] is the event id of the candidate transition to be crossed.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_an_emergency_occurs(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
   if (
      _sm.is_active("WAITING_END_OF_INITIALIZATION") && 
      ( 
         _sm.get_time() > _sm.get_last_activation_time("WAITING_END_OF_INITIALIZATION") +
         init_time_limit 
      )
   ):
      return true
   return false

## [param _sm] is the machine state.[br]
## [param _trans_id] is the transtion id of the crossed transition.[br]
## [param _nb_micro_update] is the current number of micro_update minus 1.[br]
func ef_emergency_stop(_sm: SMStateMachine, _trans_id:int, _nb_micro_update:int) -> bool:
   var stop_update = false
   print("Activation of the emmergency mode !")
   return stop_update
```

## 5) Instancing and using your state machine in your application

Now you can use the state machine in your application.

You only need to include the following code in the script associated with a
Node or a scene. For instance, in the
`addons/la-state-makina/examples/quick_start` directory, we have attached this 
code (with the code of the following section) in the `main.gd` script and we
have attached the script to a simple node within the main.tscn scene.

```plaintext
extends Node

var sm : SMStateMachine
var sm_api_ressource = preload("./music_player_api_impl.gd")
var sm_api

func _init():
   sm = SMStateMachine.new()
   sm.load_json("res://music_player.json")
   sm_api = sm_api_ressource.new()
   sm.register_api(sm_api)

func _ready():
   sm.start()

func _process(_delta: float):
   sm.update()
```

The state machine has an internal clock, so there's no need to pass the time
explicitly. However, it is possible to change that internal clock, this
functionality will be explained later (... soon).

## 6) Displaying the execution of the state machine in game (optional)

You can display the execution of a state machine by using the class 
`StateMachineEditor` in the following way :
```plaintext
extends Node

var sm : SMStateMachine
var sm_api_ressource = preload("./music_player_api_impl.gd")
var sm_api

var sm_scene : PackedScene = preload("./music_player.tscn")
var sm_viewer : StateMachineEditor

func _init():
   # Create the state machine
   sm = SMStateMachine.new()
   sm.load_json("res://music_player.json")
   sm_api = sm_api_ressource.new()
   sm.register_api(sm_api)

   # Create the state machine viewer
   sm_viewer=StateMachineEditor.new()
   sm_viewer.show_edit_button=false
   sm_viewer.size = DisplayServer.window_get_size() 
   var force_redraw = true
   var sm_name : String = "Music Player"
   sm_viewer.set_data(
      sm_scene, sm_scene.instantiate(), sm, sm_name, force_redraw
   )
   add_child(sm_viewer)

func _ready():
   sm.start()

func _process(_delta: float):
   sm.update()
   sm_viewer.show_active_states(sm.actives_states_to_uuid_states())
```

# More advanced documentation

You can consult a more complete and advanced documentation in the comments of 
the source code [sm_state_machine.gd](https://gitlab.com/Tomateauketchup/la-state-makina/-/blob/main/addons/la-state-makina/src/state_machine/sm_state_machine.gd) that
implements the machine state machinery.

You can open the documentation in the Godot editor with some nice formatting.
Start by clicking on `Script` in the Workspace. In the top-right corner of the
script editor, click on `Search Help`. Now, select the class `SMStateMachine`,
and the documentation for the state machine should open with some elegant
formatting."

# Some examples

Work in progress ...

You can consult some example in the following folder : 
```plaintext
addons/la-state-makina/examples

```

# Regarding the Documentation Writing Process

This documentation was created with the assistance of ChatGPT 3.5
(chat.openai.com - december 2023).
The used prompts were : 
 - "Traduit en anglais les phrases suivantes."
 - "You are an expert in informatics in state machina and automata. Improve
   the english of the following sentences."

# Copyright

Copyright (C) 2023 Jean_La_Tomate.\
Copyright (C) 2023 Adrien Boussicault.

All rights reserved

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

