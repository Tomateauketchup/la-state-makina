@tool
class_name UpdateListProcessor
extends RefCounted

var new_elements=[]
var to_remove_elements=[]
var not_changed_elements=[]
var changed_elements=[]

func get_uuid(object)->String:
	return ""

func set_uuid(object,uuid:String):
	pass

func is_equal(oldElem,newElem)->bool:
		return true
		
func generate_uuid()->String:
	return UuidGenerator.v4()

func update(oldTagsArray:Array,newTagsArray:Array):
	
		#On détecte maintenant les tags qui ont été créés
		if newTagsArray :		
			for t1 in newTagsArray :			
				if t1 :				
					var isCreated:bool = true
					if oldTagsArray :
						for t2 in oldTagsArray :						
							if t2 and UtilsStatic.is_equals(get_uuid(t1),get_uuid(t2)) :							
								isCreated = false;
								break;
					
					if isCreated :					
						#Au passage on lui donne un uuid si il n'en a pas
						if UtilsStatic.is_string_null_or_empty(get_uuid(t1)):					
							set_uuid(t1, generate_uuid());
						new_elements.append(t1);
					


		#On détecte les tgs qui dégagent, qui ont changé ou pas
		if oldTagsArray:		
			for oldElem in oldTagsArray:			
				if oldElem :				
					var remove:bool = true
					var hasChanged:bool = false
					var changedTag = null
					if newTagsArray:					
						for newElem in newTagsArray :						
							if newElem && UtilsStatic.is_equals(get_uuid(oldElem), get_uuid(newElem)) :							
								remove = false
								# On regarde si le tag a changé
								if is_equal(oldElem, newElem) :								
									hasChanged = false
								else :								
									hasChanged = true
									changedTag = newElem
								break;
							
					if remove :					
						to_remove_elements.append(oldElem)
					else :					
						if hasChanged :						
							changed_elements.append(changedTag)
						else :						
							not_changed_elements.append(oldElem)
