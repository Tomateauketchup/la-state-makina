@tool
class_name UtilsStatic


static func is_string_null_or_empty(str)->bool:
	return str==null or str.is_empty()
	
static func is_string_not_null_or_empty(str)->bool:
	return !is_string_null_or_empty(str)

static func is_equals(o1,o2) -> bool :	
	if o1 == null && o2 == null:
		return true;
	if o1 && o2 and o1==o2 :		
		return true		
	return false;

static func remove_end_of_string(str:String,end_to_remove:String)->String :
	if str !=null and end_to_remove != null and str.ends_with(end_to_remove):
		return str.left(str.length() - end_to_remove.length())
	return str

static func object_has_method(object:Object,method:String)->bool:
	return object != null and object.has_method(method)

static func dispose(object:Object):
	if object!=null and object_has_method(object,"dispose"):
		object.dispose()
	

# line intercept math by Paul Bourke http://paulbourke.net/geometry/pointlineplane/
# Determine the intersection point of two line segments
# Return null if the lines don't intersect. 
static func line_intersect(x1:float, y1:float, x2:float, y2:float, x3:float, y3:float, x4:float, y4:float) :
	# Check if none of the lines are of length 0
	if (x1 == x2 && y1 == y2) || (x3 == x4 && y3 == y4) :
		return null
	
	var denominator:float = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
	
	# Lines are parallel
	if denominator == 0 :
		return null
	
	var ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
	var ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

	# is the intersection along the segments
	if ua < 0 || ua > 1 || ub < 0 || ub > 1 :
		return null
	
	# Return a object with the x and y coordinates of the intersection
	var x = x1 + ua * (x2 - x1)
	var y = y1 + ua * (y2 - y1)

	return {"x":x, "y":y}

static func find_box_line_intersection(line_point_1_x:float,
	line_point_1_y:float,
	line_point_2_x:float,
	line_point_2_y:float,
	box_top_left_x:float,
	box_top_left_y:float,
	box_width:float,
	box_height:float):

	var top_left_x = box_top_left_x;
	var top_left_y = box_top_left_y;
	var top_right_x = top_left_x + box_width;
	var top_right_y = top_left_y;
	var bottom_left_x = top_left_x;
	var bottom_left_y = top_left_y + box_height;
	var bottom_right_x = top_right_x;
	var bottom_right_y = bottom_left_y;

	var intersec_top = line_intersect(
		line_point_1_x,
		line_point_1_y,
		line_point_2_x,
		line_point_2_y,
		top_left_x,
		top_left_y,
		top_right_x,
		top_right_y)

	if intersec_top != null : 
		return intersec_top

	var intersec_bottom = line_intersect(
		line_point_1_x,
		line_point_1_y,
		line_point_2_x,
		line_point_2_y,
		bottom_left_x,
		bottom_left_y,
		bottom_right_x,
		bottom_right_y)
	
	if intersec_bottom != null :
		return intersec_bottom

	var intersec_left = line_intersect(
		line_point_1_x,
		line_point_1_y,
		line_point_2_x,
		line_point_2_y,
		top_left_x,
		top_left_y,
		bottom_left_x,
		bottom_left_y)

	if intersec_left != null : 
		return intersec_left;

	var intersec_right = line_intersect(
		line_point_1_x,
		line_point_1_y,
		line_point_2_x,
		line_point_2_y,
		top_right_x,
		top_right_y,
		bottom_right_x,
		bottom_right_y)

	if intersec_right !=null :
		return intersec_right;

	return null
