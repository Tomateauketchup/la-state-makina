@tool
class_name SMEStateData
extends Node

@export var ref_uuid:String
@export var position:Vector2=Vector2(0,0)
@export var state_name:String

func _exit_tree():
	dispose()

func dispose():
	# SMDebugTools.print("dispose SMEStateData "+str(self))
	pass
	
func update_data(sm_data,editor:StateMachineEditor):	
	if sm_data is SMState :
		var name_tmp=EditorUtils.get_object_name(sm_data)
		name=name_tmp
		state_name=name_tmp	

func get_node_name()->String :
	return state_name
	


