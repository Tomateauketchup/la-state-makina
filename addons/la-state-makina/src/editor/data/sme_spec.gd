@tool
class_name SMESpec
extends Node

func _exit_tree():
	dispose()

func dispose():
	# SMDebugTools.print("dispose SMESpec "+str(self))
	pass

static func free_sme_spec(sme_spec:SMESpec):
	if sme_spec!= null :
		for c in sme_spec.get_children() :
			if c.has_method("dispose"):
				c.dispose()
			else :
				assert(c is TextEdit)
				
			c.free()
		sme_spec.dispose()
		sme_spec.free()
	sme_spec=null	

