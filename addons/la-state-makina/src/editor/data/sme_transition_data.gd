@tool
class_name SMETransitionData
extends Node

@export var ref_uuid:String
@export var transition_label:String=""
@export var transition_label_offset:Vector2=Vector2(0,0)

# if the array is not empty, the first and last point are the 
# initial and final point of the line. the point could be not define but their place in the array is reserved
@export var main_line_points:Array=[] 
@export var source_lines_points:Dictionary={}
@export var target_lines_points:Dictionary={}

func _exit_tree():
	dispose()

func dispose():
	# SMDebugTools.print("dispose SMETransitionData "+str(self))
	pass
	
func update_data(sm_data,editor:StateMachineEditor):	
	if sm_data is SMTransition :
		var name_tmp=EditorUtils.get_object_name(sm_data)
		name=name_tmp
		transition_label=name_tmp

static func clean_lines_points(lines_points:Dictionary,editor:StateMachineEditor) -> Dictionary:
	if lines_points != null and editor!= null :
		var to_remove=[]
		for l in lines_points.keys() :
			if editor.get_sm_data(l) == null :
				to_remove.append(l)
		
		for k in to_remove :
			lines_points.erase(k)
	return lines_points
	
func get_node_name()->String :
	return transition_label
