@tool
class_name StateMachineEditor
extends GraphEdit

enum GraphicalZIndex {
	TEXT_Z_INDEX=0,
	TRANSITION_LINK_Z_INDEX=1,
	TRANSITION_LABEL_Z_INDEX=2,
	STATE_NODE_Z_INDEX=3,
	TRANSITION_POINT_Z_INDEX=4
}

var show_edit_button:bool=true:
	set = set_show_edit_button
var is_editable:bool=false:
	set = set_is_editable
var machines_data:Dictionary={}
var data:SMESpec
var data_uuid:Variant
var edit_button:CheckButton=CheckButton.new()
var new_text_box:Button = Button.new()
var machine_name_label:Label=Label.new()
var machine_name:String=""
var sm_data_map={}
var sme_data_map={}
var graphical_data_map={}
var links_container:Node2D=Node2D.new()
var undo_redo_manager:EditorUndoRedoManager
var editor_interface:EditorInterface
var free_sme_spec_data_after_drawing:bool=false

func _exit_tree():
	dispose()

func dispose():
	#SMDebugTools.print("Dispose StateMachineEditor "+str(self))
	#if graphical_data_map == null :
	#	SMDebugTools.print("!! Already disposed "+str(self))

	editor_interface=null
	undo_redo_manager=null

	if machines_data != null :
		for m in machines_data.values() :
			m.dispose()

	machines_data.clear()		
	
	data = null
	data_uuid=null
	if sme_data_map != null :		
		sme_data_map.clear()
	sme_data_map=null

	if sm_data_map != null :
		sm_data_map.clear()
	sm_data_map=null	

	if graphical_data_map != null :
		for e in graphical_data_map.values() :
			if e != null and e is GraphicalTransition:
				e.dispose()
		graphical_data_map.clear()

	graphical_data_map=null
	

	SMDebugTools.print("StateMachineEditor disposed")


func _ready():
	add_child(links_container)
	minimap_enabled=false
	snapping_enabled=false

	edit_button.text="Edit"
	edit_button.pressed.connect(_on_edit_button_pressed)
	new_text_box.text = "Add TextBox"
	new_text_box.hide()
	new_text_box.pressed.connect(_on_new_text_pressed)
	scroll_offset_changed.connect(_scroll_offset_changed)

	get_menu_hbox().get_child(1).visible=false
	get_menu_hbox().get_child(2).visible=false
	get_menu_hbox().get_child(3).visible=false
	get_menu_hbox().get_child(4).visible=false
	get_menu_hbox().get_child(5).visible=false
	get_menu_hbox().get_child(7).visible=false
	get_menu_hbox().get_child(8).visible=false
	
	get_menu_hbox().add_child(edit_button)
	get_menu_hbox().add_child(new_text_box)
	get_menu_hbox().add_child(machine_name_label)
	delete_nodes_request.connect(_on_delete_node_request)

func _on_delete_node_request(node_names):
	for node_name in node_names:
		var node = find_child(node_name, false, false)
		if (node != null) and (node is GraphicalState):
			remove_state(node as GraphicalState)
		
func _on_new_text_pressed():
	var text_edit = TextEdit.new()
	text_edit.size = Vector2(100, 200)
	text_edit.set_meta("display_border", true)
	data.add_child(text_edit)
	text_edit.owner = data
	# editor_interface.edit_node(text_edit)
	editor_interface.mark_scene_as_unsaved()
	redraw()

func _on_edit_button_pressed():	
	set_is_editable(!is_editable)
	redraw()


func set_show_edit_button(show:bool):
	if edit_button != null :
		edit_button.visible=show

func set_is_editable(editable:bool):
	is_editable=editable
	if is_editable :
		new_text_box.show()
	else:
		new_text_box.hide()
			
	for c in get_children():
		if c is GraphNode :
			c.draggable=is_editable	
		elif c is TextEditWrapper:
			c.set_editable(is_editable)


func set_data(sme_spec_uuid:Variant,sme_spec:SMESpec,sm_data:SMStateMachine,sm_name:String,force_redraw:bool) :	
	if free_sme_spec_data_after_drawing == true :
		free_sme_spec_and_sm_data()
	keep_editor_data()
	machine_name=sm_name	
	if data!=sme_spec :
		force_redraw=true
	
	data=sme_spec
	data_uuid=sme_spec_uuid
	var editor_data:StateMachineEditorData=get_editor_data(sme_spec_uuid)
	if editor_data != null:
		editor_data.state_machine=sm_data
	
	_on_data_changed(force_redraw)

func keep_editor_data():
	if data_uuid != null :
		var editor_data=get_editor_data(data_uuid)
		if editor_data :
			editor_data.zoom=zoom
			editor_data.scroll_offset=scroll_offset
			editor_data.is_editable=is_editable

func get_editor_data(sme_data_uuid:Variant) -> StateMachineEditorData :
	if sme_data_uuid != null :
		var editor_data:StateMachineEditorData=machines_data.get(sme_data_uuid)
		if editor_data == null:
			is_editable=false
			editor_data=StateMachineEditorData.new()
			editor_data.zoom=zoom
			editor_data.scroll_offset=scroll_offset
			editor_data.is_editable=is_editable			
			machines_data[sme_data_uuid]=editor_data
		return editor_data
	return null

func refresh():
	_on_data_changed(true)
	
func _on_data_changed(force_redraw:bool):		
	
	if data_uuid != null :
		var editor_data:StateMachineEditorData=get_editor_data(data_uuid)
		if editor_data != null and data != null:		
			zoom=editor_data.zoom
			scroll_offset=editor_data.scroll_offset
			is_editable=editor_data.is_editable
			edit_button.button_pressed=is_editable					
			update_sme_spec_from_state_machine(data,editor_data.state_machine)	
	
	machine_name_label.text=machine_name		
	if force_redraw==true :		
		redraw()
	

func create_sme_update_list_processor()->SMEUpdateListProcessor:
	return SMEUpdateListProcessor.new()
	
func print_uuid_list(prefix:String,list:Array):
	var str="["
	for o in list:
		if o is SMEStateData or o is SMETransitionData :
			str=str+o.ref_uuid+","
		else:
			str=str+EditorUtils.get_object_uuid(o)+","
	str=str+"]"	
	print(prefix+str)

func update_sme_spec_from_state_machine(sme_spec:SMESpec,state_machine:SMStateMachine):	
	var old_states=[]
	var new_states=[]
	var old_transitions=[]
	var new_transitions=[]
	
	if sm_data_map!=null :
		sm_data_map.clear()
	
	if sme_data_map!=null :
		sme_data_map.clear()
	
	if state_machine != null :
		if state_machine.states != null:
			for c in state_machine.states:				
				new_states.append(c)
				sm_data_map[EditorUtils.get_object_uuid(c)]=c
		
		if state_machine.transitions != null:
			for c in state_machine.transitions:					
				new_transitions.append(c)
				sm_data_map[EditorUtils.get_object_uuid(c)]=c

		if state_machine.events != null:
			for c in state_machine.events:									
				sm_data_map[EditorUtils.get_object_uuid(c)]=c	
	
	if sme_spec != null :
		for c in sme_spec.get_children():		
			if c is SMEStateData :
				old_states.append(c)			
			elif c is SMETransitionData:
				old_transitions.append(c)
	
	#print_uuid_list("new_states:",new_states)
	#print_uuid_list("old_states:",old_states)
	#print_uuid_list("new_transitions:",new_transitions)
	#print_uuid_list("old_transitions:",old_transitions)
	
	var update_list_processor:UpdateListProcessor=create_sme_update_list_processor()
	
	update_list_processor.update(old_states,new_states)
	
	var to_remove_states=update_list_processor.to_remove_elements
	var to_add_states=update_list_processor.new_elements
	
	update_list_processor=create_sme_update_list_processor()
	update_list_processor.update(old_transitions,new_transitions)
	
	var to_remove_transitions=update_list_processor.to_remove_elements
	var to_add_transitions=update_list_processor.new_elements
	
	#print_uuid_list("to_remove_states:",to_remove_states)
	#print_uuid_list("to_add_states:",to_add_states)
	#print_uuid_list("to_remove_transitions:",to_remove_transitions)
	#print_uuid_list("to_add_transitions:",to_add_transitions)
	
	#We remove old ones
	for c in to_remove_states:				
		c.dispose()
		sme_spec.remove_child(c)
		c.queue_free()
	
	for c in to_remove_transitions:				
		c.dispose()
		sme_spec.remove_child(c)
		c.queue_free()
		
		
	#We create new states
	for c in to_add_states:		
		var d=SMEStateData.new()		
		sme_spec.add_child(d)
		d.set_owner(sme_spec)		
		d.ref_uuid=EditorUtils.get_object_uuid(c)		
		
	#We create new transitions
	for c in to_add_transitions:		
		var d=SMETransitionData.new()		
		sme_spec.add_child(d)
		d.set_owner(sme_spec)		
		d.ref_uuid=EditorUtils.get_object_uuid(c)	
		
	
	
	#We map nodes data together
	for c in sme_spec.get_children():
		if c is SMEStateData or c is SMETransitionData:
			sme_data_map[c.ref_uuid]=c
			update_data(c.ref_uuid)		


func get_sme_data(uuid:String)->Node:
	return sme_data_map.get(uuid)

func get_sm_data(uuid:String):
	return sm_data_map.get(uuid)

func get_graphical_data(uuid:String):
	return graphical_data_map.get(uuid)
	

func update_data(uuid:String):	
	var sm_data=get_sm_data(uuid)
	if sm_data != null :		
		var sm_name=EditorUtils.get_object_name(sm_data)		
		var sme_data=get_sme_data(uuid)						
		if sme_data !=null :						
			sme_data.update_data(sm_data,self)			
			
		var graphical_data=get_graphical_data(uuid)
		if graphical_data != null :			
			var editor_data:StateMachineEditorData=get_editor_data(data_uuid)
			graphical_data.update_data(sme_data,sm_data, editor_data.state_machine)
			
	



class TextEditWrapper extends ColorRect :
	var original : TextEdit
	var original_position : Vector2
	var original_size : Vector2
	var wrapper_text : TextEdit
	var margin : int
	var border_color_hide : Color
	var border_color_show : Color
	var corner_color : Color
	var NW : ColorRect
	var SE : ColorRect
	var line : Line2D
	var editable : bool
	var is_focus : bool
	var add_border : ColorRect
	var state_machine_editor : StateMachineEditor
	var font_size_edit : SpinBox
	var display_border : bool

	func dispose():
		if original == null:
			return
		original = null
		self.queue_free()
		wrapper_text = null
		NW = null
		SE = null
		line = null
		add_border = null
		state_machine_editor = null
		font_size_edit = null

	func define_border_color():
		if display_border:
			line.default_color = border_color_show
			add_border.color = border_color_hide
		else:
			line.default_color = border_color_hide
			add_border.color = border_color_show

	func swap_display_border():
		display_border = not(original.get_meta("display_border"))
		original.set_meta("display_border",  display_border)
		define_border_color()
		notify_is_modified()
		
	func update_line():
		line.clear_points()
		line.add_point(Vector2(margin/2, margin/2))
		line.add_point(Vector2(size.x - margin/2, margin/2))
		line.add_point(Vector2(size.x - margin/2, size.y - margin/2))
		line.add_point(Vector2(margin/2, size.y - margin/2))
		line.add_point(Vector2(margin/2, margin/2))

	func set_editable(is_editable:bool):
		editable = is_editable
		if is_editable :
			wrapper_text.editable = true
			add_border.show()
			NW.show()
			SE.show()
			line.show()
			font_size_edit.show()
		else:
			wrapper_text.editable = false
			add_border.hide()
			NW.hide()
			SE.hide()
			font_size_edit.hide()
			if display_border:
				line.show()
			else:
				line.hide()

	func _init(richText:TextEdit, _state_machine_editor, _editable:bool):
		state_machine_editor = _state_machine_editor
		original = richText
		editable = _editable
		is_focus = false
		
		# Define the Rectangle container
		color =  Color(0, 0, 0, 0)
		original_position = Vector2(original.position)
		original_size = Vector2(original.size)
		position = Vector2(original_position)
		size = Vector2(original_size)
		focus_mode = FocusMode.FOCUS_CLICK
		mouse_default_cursor_shape = CursorShape.CURSOR_DRAG 
		connect("gui_input", _on_gui_input_cadre)
		connect("resized", _on_resized)
		connect("focus_entered", _on_focus_entered)
		connect("focus_exited", _on_focus_exited)
		
		# Define the border
		if not original.has_meta("display_border"):
			original.set_meta("display_border", false)
		display_border = original.get_meta("display_border")
		margin = 7
		border_color_hide = Color.ORANGE
		border_color_show = Color.BLUE
		line = Line2D.new()
		line.width = margin
		update_line()
		add_child(line)
		
		
		# Define the add border button
		add_border  = ColorRect.new()
		add_border.color = border_color_hide
		add_border.size = Vector2(2*margin, 2*margin)
		add_border.position =Vector2(0, size.y + margin )
		add_border.connect("gui_input", _on_gui_input_add_border)
		add_child(add_border)
		
		# Define the line Edit for ths font
		font_size_edit = SpinBox.new()
		font_size_edit.min_value = 1
		font_size_edit.max_value = 100
		var text_font_size : int = original.get_theme_font_size("font_size")
		font_size_edit.value = text_font_size
		font_size_edit.suffix = " px"
		font_size_edit.position = Vector2(margin*4, size.y + margin)
		add_child(font_size_edit)
		font_size_edit.connect("value_changed", _on_font_size_edit_changed)
		
		define_border_color()
		
		# Define the corners
		corner_color = Color.YELLOW
		
		NW = ColorRect.new()
		NW.color = corner_color
		NW.size = Vector2(margin, margin)
		NW.mouse_default_cursor_shape = CursorShape.CURSOR_FDIAGSIZE
		NW.connect("gui_input", _on_gui_input_NW)
		add_child(NW)
		
		SE = ColorRect.new()
		SE.color = corner_color
		SE.size = Vector2(margin, margin)
		SE.position = original_size - Vector2(margin, margin)
		SE.mouse_default_cursor_shape = CursorShape.CURSOR_FDIAGSIZE
		SE.connect("gui_input", _on_gui_input_SE)
		add_child(SE)
		
		# Define the text inside the container
		wrapper_text = TextEdit.new()
		wrapper_text.text = String(original.text)
		wrapper_text.size = size - 2*Vector2(margin, margin)
		wrapper_text.position = Vector2(margin, margin)
		wrapper_text.focus_mode = FocusMode.FOCUS_CLICK
		wrapper_text.add_theme_font_size_override("font_size", text_font_size)
		var normal_style : StyleBoxFlat = StyleBoxFlat.new()
		var focus_style : StyleBoxFlat = StyleBoxFlat.new()
		var read_only_style : StyleBoxFlat = StyleBoxFlat.new()
		
		normal_style.bg_color = Color(0, 0, 0, 0)
		wrapper_text.add_theme_stylebox_override("normal", normal_style)
		focus_style.bg_color = Color(0, 0, 0, 0)
		focus_style.border_color = Color(0, 1, 0, 1)
		focus_style.set_border_width_all(2)
		focus_style.set_corner_radius_all(2)
		wrapper_text.add_theme_stylebox_override("focus", focus_style)
		read_only_style.bg_color = Color(0, 0, 0, 0)
		wrapper_text.add_theme_stylebox_override("read_only", read_only_style)
		
		add_child(wrapper_text)
		wrapper_text.connect("text_changed", _on_text_changed)
		
		set_editable(editable)

	func update_original_geometry():
		if is_instance_valid(original):
			original.size = original_size
			original.position = original_position
			notify_is_modified()
		else:
			push_error("Original should be valid")

	func _on_font_size_edit_changed(value:float):
		original.add_theme_font_size_override("font_size", int(value))
		wrapper_text.add_theme_font_size_override("font_size", int(value))
		notify_is_modified()
	
	func _on_resized():
		SE.position = original_size - Vector2(margin, margin)
		wrapper_text.size = size - 2*Vector2(margin, margin)
		add_border.position =Vector2(0, size.y + margin )
		font_size_edit.position = Vector2(margin*4, size.y + margin)
		update_line()
	
	var clicked_on_cadre : bool
	func _on_gui_input_cadre(event:InputEvent):
		if not editable:
			return
		if event is InputEventMouseButton:
			if event.pressed:
				clicked_on_cadre = true
			else:
				clicked_on_cadre = false
		if event is InputEventMouseMotion:
			if clicked_on_cadre:
				original_position += event.relative
				position += event.relative * scale
				update_original_geometry()


	var clicked_on_NW : bool
	func _on_gui_input_NW(event:InputEvent):
		if not editable:
			return
		if event is InputEventMouseButton:
			if event.pressed:
				clicked_on_NW = true
			else:
				clicked_on_NW = false
		if event is InputEventMouseMotion:
			if clicked_on_NW:
				original_size -= event.relative
				if original_size.x < 4*margin:
					original_size.x = 4*margin
				if original_size.y < 4*margin:
					original_size.y = 4*margin
				size = Vector2(original_size)
				original_position += event.relative
				position += event.relative * scale
				update_original_geometry()

	func _on_gui_input_add_border(event:InputEvent):
		if not editable:
			return
		if event is InputEventMouseButton:
			if event.pressed:
				swap_display_border()

	var clicked_on_SE : bool
	func _on_gui_input_SE(event:InputEvent):
		if not editable:
			return
		if event is InputEventMouseButton:
			if event.pressed:
				clicked_on_SE = true
			else:
				clicked_on_SE = false
		if event is InputEventMouseMotion:
			if clicked_on_SE:
				original_size += event.relative
				if original_size.x < 4*margin:
					original_size.x = 4*margin
				if original_size.y < 4*margin:
					original_size.y = 4*margin
				size = Vector2(original_size)
				update_line()
	
	func _on_focus_entered():
		is_focus = true
	
	func _on_focus_exited():
		is_focus = false
	
	func _on_text_changed():
		if original != null  and is_instance_valid(original):
			original.text = String(wrapper_text.text)
			# state_machine_editor.editor_interface.edit_node(original)
			state_machine_editor.editor_interface.mark_scene_as_unsaved()
			notify_is_modified()
	
	func notify_is_modified():
		state_machine_editor.editor_interface.mark_scene_as_unsaved()

func redraw():
	#SMDebugTools.print("redraw")
	for c in graphical_data_map.values():
		if c is GraphicalTransition :
			c.dispose()
	
	graphical_data_map.clear()

	for c in get_children():
		if c != links_container :
			c.queue_free()	

	if data !=null :
		for c in data.get_children():
			if c is SMEStateData :
				var state:SMEStateData=c
				var graphical_state : GraphicalState =create_graphical_state()
				add_child(graphical_state)
				graphical_state.draggable=is_editable
				graphical_state.ref_uuid=state.ref_uuid
				graphical_data_map[state.ref_uuid]=graphical_state
				update_data(state.ref_uuid)
			if c is TextEdit :
				add_child(TextEditWrapper.new(c, self, is_editable))

		#Second pass for links
		for c in data.get_children():
			if c is SMETransitionData :
				var transition_data:SMETransitionData=c
				var elem=create_graphical_transition()
				elem.ref_uuid=transition_data.ref_uuid
				graphical_data_map[transition_data.ref_uuid]=elem
				update_data(transition_data.ref_uuid)
	
	# On fait un call defered ici pour laisser le temps à godot de calculer les tailles des
	# boites des états.
	call_deferred("redraw_links")
	

func redraw_links():
	#SMDebugTools.print("redraw_links")

	if data != null :
		for c in data.get_children():			
			if c is SMETransitionData :			
				var sme_transition_data:SMETransitionData=c
				var graphical_transition:GraphicalTransition=get_graphical_data(sme_transition_data.ref_uuid)			
				if graphical_transition != null :				
					graphical_transition.draw_lines()
	
	if free_sme_spec_data_after_drawing == true :
		free_sme_spec_and_sm_data()

func free_sme_spec_and_sm_data():

	SMESpec.free_sme_spec(data)
	data=null

	if sm_data_map!=null :
		sm_data_map.clear()
	
	if sme_data_map!=null :
		sme_data_map.clear()

func _scroll_offset_changed(offset: Vector2):
	links_container.scale=Vector2(1,1)*zoom
	links_container.position=-scroll_offset
	for c in get_children():
		if c is TextEditWrapper:
			c.scale = Vector2(1,1)*zoom
			c.position = c.original_position*zoom - scroll_offset
	#redraw_links()

func create_graphical_state()->GraphicalState:
	var graphical_state=GraphicalState.new()	
	graphical_state.editor=self	
	return graphical_state

func create_graphical_transition()->GraphicalTransition:
	var elem=GraphicalTransition.new()	
	elem.editor=self	
	return elem

func show_active_states(state_uuids):
	if state_uuids != null :
		if graphical_data_map != null :
			for key in graphical_data_map.keys() :
				var gs=graphical_data_map.get(key)
				if gs is GraphicalState :
					var is_active:bool=state_uuids.has(key)
					gs.setBoxStyle(is_active)

func remove_state(graphical_state:GraphicalState):
#	# graphical_state.sm_state.queue_free()
#	# graphical_state.queue_free()
	push_warning("Remove Graphical State is not yet implemented !")

func get_undo_redo_manager()->EditorUndoRedoManager:
	return null
	
func do_undo_redo_state_position_changed(uuid:String,new_position:Vector2,old_position:Vector2):	
	var object:SMEStateData=get_sme_data(uuid)	
	
	if undo_redo_manager != null :
		undo_redo_manager.create_action("state_moved")
		undo_redo_manager.add_do_property(object,"position",new_position)
		undo_redo_manager.add_undo_property(object,"position",old_position)
		undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",uuid)
		undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",uuid)
		undo_redo_manager.commit_action()
	else :
		object.position = new_position
		do_and_undo_method_for_position_changed(uuid)
	

func do_and_undo_method_for_position_changed(uuid:String):
	update_data(uuid)
	redraw_links()


func _can_drop_data(at_position, data) -> bool :
	if data != null and (data is GraphicalTransition.LinePoint or data is GraphicalTransition.LabelsBox) :
		return true
	return false
	
func _drop_data(at_position, data):
	#print("node droped at:"+str(at_position))
	if data is GraphicalTransition.LinePoint :
		var line_point:GraphicalTransition.LinePoint=data
		if line_point.get_parent() and line_point.get_parent() is CanvasItem :				
			var point_position=line_point.get_parent().get_local_mouse_position()
			modify_point_position(point_position,line_point)
	elif data is GraphicalTransition.LabelsBox :
		var label_box:GraphicalTransition.LabelsBox=data
		if label_box.get_parent() and label_box.get_parent() is CanvasItem :
			var point_position=label_box.get_parent().get_local_mouse_position()
			modify_label_box_position(point_position,label_box)

func modify_label_box_position(new_position:Vector2,label_box:GraphicalTransition.LabelsBox) :
	var sme_data=get_sme_data(label_box.transition_ref_uuid)
	if sme_data is SMETransitionData :
		var sme_transition:SMETransitionData=sme_data

		var new_offset=new_position-label_box.initial_center_position
		var old_offset=sme_data.transition_label_offset

		if undo_redo_manager != null :
			undo_redo_manager.create_action("labels_boxx_moved")
			undo_redo_manager.add_do_property(sme_transition,"transition_label_offset",new_offset)
			undo_redo_manager.add_undo_property(sme_transition,"transition_label_offset",old_offset)
			undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
			undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
			undo_redo_manager.commit_action()
		else :
			sme_transition.transition_label_offset = new_offset
			do_and_undo_method_for_position_changed(sme_data.ref_uuid)

	
func modify_point_position(new_position:Vector2,line_point:GraphicalTransition.LinePoint):
		var sme_data=get_sme_data(line_point.transition_ref_uuid)
		if sme_data is SMETransitionData :
			var sme_transition:SMETransitionData=sme_data
			if line_point.is_first_main_point or line_point.is_last_main_point or line_point.is_inside_main_point :
				
				var old_main_line_points=sme_transition.main_line_points.duplicate()
				var new_main_line_points=sme_transition.main_line_points.duplicate()

				if line_point.is_first_main_point :
					if new_main_line_points.is_empty() :
						new_main_line_points.append(new_position)
						new_main_line_points.append(null)#last main point
					else : new_main_line_points[0]=new_position
				elif line_point.is_last_main_point :
					if new_main_line_points.is_empty() :
						new_main_line_points.append(null) #place of first main point
						new_main_line_points.append(null)
					new_main_line_points[new_main_line_points.size()-1]=new_position		
				elif line_point.is_inside_main_point :
					var idx=new_main_line_points.find(line_point.center_point)
					if idx>0 : #normalement le noeud initial est traité avant
						new_main_line_points[idx]=new_position
				
				
				#print("old:"+str(old_main_line_points))
				#print("new:"+str(new_main_line_points))
				if undo_redo_manager != null :
					undo_redo_manager.create_action("point_moved")
					undo_redo_manager.add_do_property(sme_transition,"main_line_points",new_main_line_points)
					undo_redo_manager.add_undo_property(sme_transition,"main_line_points",old_main_line_points)
					undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
					undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
					undo_redo_manager.commit_action()
				else :
					sme_transition.main_line_points=new_main_line_points
					do_and_undo_method_for_position_changed(sme_data.ref_uuid)

			elif UtilsStatic.is_string_not_null_or_empty(line_point.source_line_point) or UtilsStatic.is_string_not_null_or_empty(line_point.target_line_point) :
				
				var line:Array		
				var old_line={}
				var new_line={}
				var poperty_to_change=""
				if UtilsStatic.is_string_not_null_or_empty(line_point.source_line_point) :					
					old_line=sme_transition.source_lines_points.duplicate()
					new_line=sme_transition.source_lines_points.duplicate()
					poperty_to_change="source_lines_points"
					line=new_line[line_point.source_line_point]					
				elif UtilsStatic.is_string_not_null_or_empty(line_point.target_line_point) :					
					old_line=sme_transition.target_lines_points.duplicate()
					new_line=sme_transition.target_lines_points.duplicate()
					poperty_to_change="target_lines_points"
					line=new_line[line_point.target_line_point]		
				
				
				if line!=null :
					var idx=0
					for p in line :
						if p == line_point.center_point :
							line[idx]=new_position
							break
						idx=idx+1

				#print("old:"+str(old_line))
				#print("new:"+str(new_line))
				new_line=SMETransitionData.clean_lines_points(new_line,self)
				if undo_redo_manager != null :
					undo_redo_manager.create_action("point_moved")
					undo_redo_manager.add_do_property(sme_transition,poperty_to_change,new_line)
					undo_redo_manager.add_undo_property(sme_transition,poperty_to_change,old_line)
					undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
					undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_data.ref_uuid)
					undo_redo_manager.commit_action()
				else :
					if poperty_to_change == "source_lines_points" :
						sme_transition.source_lines_points=new_line
					elif poperty_to_change == "target_lines_points" :
						sme_transition.target_lines_points=new_line
					
					do_and_undo_method_for_position_changed(sme_data.ref_uuid)
					

func add_point_to_line(transition_uuid:String,line:GraphicalTransition.LineContainer,point:Vector2):
	
	var sme_transition:SMETransitionData=get_sme_data(transition_uuid)
	if sme_transition != null :		
		var multi_line:GraphicalTransition.MultiLine=line.multi_line
		var point_to_find=line.source_point.center_point
		if multi_line.is_main_line :						
			
			var old_points_line=sme_transition.main_line_points.duplicate()
			var new_points_line=sme_transition.main_line_points.duplicate()
			var idx=new_points_line.find(point_to_find)
			
			if new_points_line.size()==1 :				
				new_points_line.append(null) #we add the last node because we know we will ad the new one
			if new_points_line.is_empty() :				
				new_points_line.append(null)#at least the source and last point
				new_points_line.append(null)
			if idx==-1 or idx==0 :				
				#We add the new point just after the source point
				new_points_line.insert(1,point)
			else:				
				new_points_line.insert(idx+1,point)
			
			
			#print("old:"+str(old_points_line))
			#print("new:"+str(new_points_line))
			if undo_redo_manager != null :
				undo_redo_manager.create_action("point_created")
				undo_redo_manager.add_do_property(sme_transition,"main_line_points",new_points_line)
				undo_redo_manager.add_undo_property(sme_transition,"main_line_points",old_points_line)
				undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.commit_action()
			else :
				sme_transition.main_line_points=new_points_line
				do_and_undo_method_for_position_changed(sme_transition.ref_uuid)

		elif UtilsStatic.is_string_not_null_or_empty(multi_line.source_line_state_uuid) or UtilsStatic.is_string_not_null_or_empty(multi_line.target_line_state_uuid) :
			
			var state_uuid=""
			var new_lines_points={}
			var old_lines_points={}
			var property=""
			if UtilsStatic.is_string_not_null_or_empty(multi_line.source_line_state_uuid) :				
				new_lines_points=sme_transition.source_lines_points.duplicate()
				property="source_lines_points"
				state_uuid=multi_line.source_line_state_uuid
			elif UtilsStatic.is_string_not_null_or_empty(multi_line.target_line_state_uuid) :				
				new_lines_points=sme_transition.target_lines_points.duplicate()
				property="target_lines_points"
				state_uuid=multi_line.target_line_state_uuid
			
			
			old_lines_points=new_lines_points.duplicate()			
			var line_pt=new_lines_points.get(state_uuid)			
			if line_pt == null:
				line_pt=[]
				new_lines_points[state_uuid]=line_pt
			if line_pt != null :
				var idx=line_pt.find(point_to_find)
				if idx==-1 :
					line_pt.insert(0,point)
				else :
					line_pt.insert(idx+1,point)

			new_lines_points=SMETransitionData.clean_lines_points(new_lines_points,self)
			#print("old:"+str(old_lines_points))
			#print("new:"+str(new_lines_points))
			if undo_redo_manager != null :
				undo_redo_manager.create_action("point_created")
				undo_redo_manager.add_do_property(sme_transition,property,new_lines_points)
				undo_redo_manager.add_undo_property(sme_transition,property,old_lines_points)
				undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.commit_action()
			else :
				if property=="source_lines_points" :
					sme_transition.source_lines_points=new_lines_points
				elif property=="target_lines_points" :
					sme_transition.target_lines_points=new_lines_points
				
				do_and_undo_method_for_position_changed(sme_transition.ref_uuid)

func remove_point_from_line(line_point:GraphicalTransition.LinePoint):
	var point_to_remove=line_point.center_point
	var sme_transition=get_sme_data(line_point.transition_ref_uuid)
	if sme_transition!=null and sme_transition is SMETransitionData :
		if line_point.is_inside_main_point == true :
			var new_main_line_points=sme_transition.main_line_points.duplicate()
			var old_main_line_points=sme_transition.main_line_points.duplicate()
			new_main_line_points.erase(point_to_remove)

			if undo_redo_manager != null :
				undo_redo_manager.create_action("remove_point")
				undo_redo_manager.add_do_property(sme_transition,"main_line_points",new_main_line_points)
				undo_redo_manager.add_undo_property(sme_transition,"main_line_points",old_main_line_points)
				undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.commit_action()
			else :
				sme_transition.main_line_points=new_main_line_points
				do_and_undo_method_for_position_changed(sme_transition.ref_uuid)
		
		elif UtilsStatic.is_string_not_null_or_empty(line_point.source_line_point) or UtilsStatic.is_string_not_null_or_empty(line_point.target_line_point) :
			var line:Array		
			var old_line={}
			var new_line={}
			var poperty_to_change=""
			if UtilsStatic.is_string_not_null_or_empty(line_point.source_line_point) :					
				old_line=sme_transition.source_lines_points.duplicate()
				new_line=sme_transition.source_lines_points.duplicate()
				poperty_to_change="source_lines_points"
				line=new_line[line_point.source_line_point]					
			elif UtilsStatic.is_string_not_null_or_empty(line_point.target_line_point) :					
				old_line=sme_transition.target_lines_points.duplicate()
				new_line=sme_transition.target_lines_points.duplicate()
				poperty_to_change="target_lines_points"
				line=new_line[line_point.target_line_point]		
				
				
			if line!=null :
				line.erase(point_to_remove)				

			#print("old:"+str(old_line))
			#print("new:"+str(new_line))
			new_line=SMETransitionData.clean_lines_points(new_line,self)
			if undo_redo_manager != null :				
				undo_redo_manager.create_action("remove_point")
				undo_redo_manager.add_do_property(sme_transition,poperty_to_change,new_line)
				undo_redo_manager.add_undo_property(sme_transition,poperty_to_change,old_line)
				undo_redo_manager.add_do_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.add_undo_method(self,"do_and_undo_method_for_position_changed",sme_transition.ref_uuid)
				undo_redo_manager.commit_action()
			else :
				if poperty_to_change == "source_lines_points" :
					sme_transition.source_lines_points=new_line
				elif poperty_to_change == "target_lines_points" :
					sme_transition.target_lines_points=new_line
				
				do_and_undo_method_for_position_changed(sme_transition.ref_uuid)

func highlight_transition(transition_ref_uuid:String,highlight:bool):

	if links_container ==null : return
	
	for o in links_container.get_children() :
		if o is GraphicalTransition.LineContainer and o.transition_ref_uuid == transition_ref_uuid :
			var line:Line2D=o.line
			if line != null :
				if highlight == true :
					line.default_color=GraphicalTransition.highlight_links_color
				else :
					line.default_color=GraphicalTransition.links_color




				

func node_selected(uuid:String):	
	selection_changed()
	
func node_deselected(uuid:String):
	selection_changed()

func selection_changed():
	var selected_nodes:Array[Node]=[]
	for c in get_children():
		if c is GraphicalState :			
			var gs:GraphicalState=c
			if gs.selected:
				var o=get_sme_data(c.ref_uuid)
				if o != null :
					selected_nodes.append(o)
	if selected_nodes.is_empty() :
		selected_nodes.append(data)
	
	if editor_interface != null :
		editor_interface.get_selection().clear()	
		for nd in selected_nodes :		
			editor_interface.get_selection().add_node(nd)
		


func get_background_color()-> Color:
	var sb:StyleBox=get_theme_stylebox("bg")
	if sb != null and sb is StyleBoxFlat :
		return sb.bg_color
	return Color.BLACK
