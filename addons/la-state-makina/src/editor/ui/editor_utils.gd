@tool
class_name EditorUtils
extends RefCounted

static func get_object_uuid(object)->String:
	if object is SMState : return str(object.uuid)
	elif object is SMTransition : return str(object.uuid)
	elif object is SMEvent : return str(object.name)	
	return ""

static func get_object_name(object)->String:	
	if object is SMState : return object.name
	elif object is SMTransition : return object.event+"_|_"+str(object.uuid)
	elif object is SMEvent : return str(object.name)
	return ""
