@tool
class_name GraphicalTransition
extends RefCounted

const links_color:Color=Color(0.8,0.8,0.8,1)
const highlight_links_color:Color=Color.RED
const arrows_color:Color=links_color
const points_color:Color=links_color
const points_color_edit_mode:Color=Color.WHITE

var editor:StateMachineEditor
var ref_uuid:String
var multi_line_array:Array[MultiLine]=[]
var points_containers:Array[LinePoint]=[]

func dispose():
	#SMDebugTools.print("dispose GraphicalTransition "+str(self),"dispose")
	if editor == null :
		SMDebugTools.print("!! Already disposed "+str(self))
	remove_shapes_from_editor()
	editor=null

func update_data(sme_data:SMETransitionData, sm_data:SMTransition, _sm_machine_data:SMStateMachine):
	pass

func remove_shapes_from_editor():
	for p in points_containers :		
		p.queue_free()
	for p in multi_line_array :
		p.dispose()
		

func add_shape_to_editor(shape:CanvasItem):
	editor.links_container.add_child(shape)	

func draw_lines():

	remove_shapes_from_editor()
	points_containers.clear()
	multi_line_array.clear()

	
	var sme_transition_data:SMETransitionData=editor.get_sme_data(ref_uuid)
	var sm_transition_data:SMTransition=editor.get_sm_data(ref_uuid)	
	var event_name=sm_transition_data.event
	var sm_event_data:SMEvent=editor.get_sm_data(event_name)
	if not sme_transition_data :
		return
	if not sm_transition_data :
		return		
	
	#we find the points couples
	var source_sme_state_array:Array[SMEStateData]=[]
	var target_sme_state_array:Array[SMEStateData]=[]

	var source_graphical_state_array:Array[GraphicalState]=[]
	var target_graphical_state_array:Array[GraphicalState]=[]

	var sources:Array[StringName]=sm_transition_data.origin_uuids
	for o in sources :
		source_sme_state_array.append(editor.get_sme_data(o))
		source_graphical_state_array.append(editor.get_graphical_data(o))

	var targets:Array[StringName]=sm_transition_data.end_uuids
	for o in targets :
		target_sme_state_array.append(editor.get_sme_data(o))
		target_graphical_state_array.append(editor.get_graphical_data(o))

	#We calculate thee default first main point
	
	var first_default_main_point=calculate_barycenter_of_states(source_sme_state_array)
	var last_default_main_point=calculate_barycenter_of_states(target_sme_state_array)

	var first_main_point=null
	var last_main_point=null

	var main_line_points=sme_transition_data.main_line_points

	var main_line_inner_points:Array=[]

	
	if main_line_points.size()>0 :		
		first_main_point=main_line_points[0]
		if main_line_points.size()>1 :		
			last_main_point=main_line_points[main_line_points.size()-1]
			for idx in range(1,main_line_points.size()-1) :
				main_line_inner_points.append(main_line_points[idx])
		
		
	if first_main_point == null :
		first_main_point=first_default_main_point
	if last_main_point == null :
		last_main_point=last_default_main_point

	
	var first_main_line_point:LinePoint=LinePoint.new()
	first_main_line_point.editor=editor
	first_main_line_point.set_point_position(first_main_point)
	first_main_line_point.is_first_main_point=true
	first_main_line_point.transition_ref_uuid=ref_uuid
	var last_main_line_point:LinePoint=LinePoint.new()
	last_main_line_point.editor=editor
	last_main_line_point.set_point_position(last_main_point)
	last_main_line_point.is_last_main_point=true
	last_main_line_point.transition_ref_uuid=ref_uuid

	var multi_line:MultiLine = MultiLine.new()
	multi_line.editor=editor
	multi_line.transition_ref_uuid=ref_uuid
	multi_line.points.append(first_main_line_point)

	var point_to_recalulate:Dictionary={}
	var add_point_for_recalculation = func(state_uuid:String,state_point:LinePoint,outside_point:LinePoint):
		var ar=point_to_recalulate.get(state_uuid)
		if ar == null :
			ar=[]
			point_to_recalulate[state_uuid]=ar
		var d={}
		d["state_point"]=state_point
		d["outside_point"]=outside_point
		ar.append(d)

	#Cas de l'état qui boucle sur lui même
	if first_main_line_point.center_point == last_main_line_point.center_point and main_line_inner_points.size() == 0 and source_graphical_state_array.size()>0 :
		var graphical_state:GraphicalState=source_graphical_state_array[0]
		multi_line.is_loop_line=true
		var offset=40
		var top_right_point=graphical_state.position_offset+Vector2(graphical_state.size.x,0)
		#var top_right_point=graphical_state.position_offset
		first_main_line_point.set_point_position(top_right_point-Vector2(offset,0))
		first_main_line_point.is_inside_loop_line=true
		last_main_line_point.set_point_position(top_right_point-Vector2(0,-offset))
		last_main_line_point.is_inside_loop_line=true
		
		var add_point_var = func(point:Vector2):		
			var main_line_point:LinePoint=LinePoint.new()
			main_line_point.is_inside_loop_line=true
			main_line_point.editor=editor
			main_line_point.transition_ref_uuid=ref_uuid
			main_line_point.is_inside_main_point=true
			main_line_point.set_point_position(point)
			multi_line.points.append(main_line_point)
			points_containers.append(main_line_point)
		
		add_point_var.call(top_right_point+Vector2(-offset,-offset))
		add_point_var.call(top_right_point+Vector2(offset,-offset))
		add_point_var.call(top_right_point+Vector2(offset,offset))
	
	
	for p in main_line_inner_points :
		var main_line_point:LinePoint=LinePoint.new()
		main_line_point.editor=editor
		main_line_point.transition_ref_uuid=ref_uuid
		main_line_point.is_inside_main_point=true
		main_line_point.set_point_position(p)
		multi_line.points.append(main_line_point)
		points_containers.append(main_line_point)

	multi_line.points.append(last_main_line_point)		
	multi_line_array.append(multi_line)
	multi_line.is_main_line=true
	multi_line.event_name=event_name

	points_containers.append(first_main_line_point)
	points_containers.append(last_main_line_point)

	if multi_line.is_loop_line == false :
		if source_sme_state_array.size()==1 and multi_line.points.size()>1 :
			multi_line.points[0].force_invisibility=true
			add_point_for_recalculation.call(sources[0],multi_line.points[0],multi_line.points[1])
		
		if target_sme_state_array.size()==1 and multi_line.points.size()>1:
			multi_line.points[multi_line.points.size()-1].force_invisibility=true
			add_point_for_recalculation.call(targets[0],multi_line.points[multi_line.points.size()-1],multi_line.points[multi_line.points.size()-2])


	if source_sme_state_array.size()>1 :
		first_main_line_point.is_choice_point=true
		var type=""
		if sm_transition_data.type == SMTransition.TransitionType.AND :
			type="AND"
		elif sm_transition_data.type == SMTransition.TransitionType.OR :
			type="OR"
		first_main_line_point.choice_type=type
		for sme_state in source_sme_state_array :		
			var p=calculate_center_of_state(sme_state)
			var line_point:LinePoint=LinePoint.new()	
			line_point.editor=editor	
			line_point.transition_ref_uuid=ref_uuid
			line_point.set_point_position(p)
			line_point.is_state_point=true
			points_containers.append(line_point)

			multi_line = MultiLine.new()
			multi_line.editor=editor
			multi_line.points.append(line_point)
			multi_line.transition_ref_uuid=ref_uuid

			var line_points=sme_transition_data.source_lines_points.get(sme_state.ref_uuid)
			if line_points!=null :
				for pt in line_points :
					line_point=LinePoint.new()		
					line_point.editor=editor	
					line_point.transition_ref_uuid=ref_uuid
					line_point.set_point_position(pt)
					line_point.source_line_point=sme_state.ref_uuid
					points_containers.append(line_point)
					multi_line.points.append(line_point)


			multi_line.points.append(first_main_line_point)
			multi_line_array.append(multi_line)
			multi_line.source_line_state_uuid=sme_state.ref_uuid
				
			if multi_line.points.size()>1 :
				add_point_for_recalculation.call(sme_state.ref_uuid,multi_line.points[0],multi_line.points[1])
		

	if target_sme_state_array.size()>1 :
		for sme_state in target_sme_state_array :		
			var p=calculate_center_of_state(sme_state)
			var line_point:LinePoint=LinePoint.new()		
			line_point.editor=editor	
			line_point.transition_ref_uuid=ref_uuid		
			line_point.set_point_position(p)
			line_point.is_state_point=true
			points_containers.append(line_point)

			var state_point=line_point
			
			multi_line = MultiLine.new()
			multi_line.editor=editor
			multi_line.points.append(last_main_line_point)
			multi_line.transition_ref_uuid=ref_uuid

			var line_points=sme_transition_data.target_lines_points.get(sme_state.ref_uuid)
			if line_points!=null :
				for pt in line_points :
					line_point=LinePoint.new()		
					line_point.editor=editor	
					line_point.transition_ref_uuid=ref_uuid
					line_point.set_point_position(pt)
					line_point.target_line_point=sme_state.ref_uuid
					points_containers.append(line_point)
					multi_line.points.append(line_point)

			multi_line.points.append(state_point)		
			multi_line_array.append(multi_line)
			multi_line.target_line_state_uuid=sme_state.ref_uuid
		
			if multi_line.points.size()>1 :
				add_point_for_recalculation.call(sme_state.ref_uuid,multi_line.points[multi_line.points.size()-1],multi_line.points[multi_line.points.size()-2])
	

	# On recalcule les points pour qu'il touchent les bord des boites
	for state_uuid in point_to_recalulate.keys() :
		var points_tmp:Array=point_to_recalulate[state_uuid]
		var graphical_state=editor.get_graphical_data(state_uuid)
		if graphical_state != null :
			for p_tmp in points_tmp :
				var state_point:LinePoint=p_tmp["state_point"]
				var outside_point:LinePoint=p_tmp["outside_point"]
				var line_point_1_x:float=state_point.center_point.x
				var line_point_1_y:float=state_point.center_point.y
				var line_point_2_x:float=outside_point.center_point.x
				var line_point_2_y:float=outside_point.center_point.y
				var box_top_left_x:float=graphical_state.position_offset.x
				var box_top_left_y:float=graphical_state.position_offset.y
				var box_width:float=graphical_state.size.x
				var box_height:float=graphical_state.size.y
				var new_point=UtilsStatic.find_box_line_intersection(line_point_1_x,
					line_point_1_y,
					line_point_2_x,
					line_point_2_y,
					box_top_left_x,
					box_top_left_y,
					box_width,
					box_height)
				if new_point != null :
					state_point.set_point_position(Vector2(new_point.x,new_point.y))


	for o in multi_line_array :		
		o.draw_lines(sme_transition_data,sm_transition_data,sm_event_data,self)
	
	for p in points_containers :		
		add_shape_to_editor(p)


func calculate_center_of_state(sme_state:SMEStateData)->Vector2:
	var graphical_state:GraphicalState=editor.get_graphical_data(sme_state.ref_uuid)
	if graphical_state:
		return sme_state.position+graphical_state.size/2
	
	return Vector2(0,0)
	
func calculate_barycenter_of_states(sme_state_array:Array[SMEStateData])->Vector2:
	var min_x
	var max_x
	var min_y
	var max_y

	
	for state in sme_state_array :
		var graphical_state:GraphicalState=editor.get_graphical_data(state.ref_uuid)
		if graphical_state:
			var pos=state.position+graphical_state.size/2
			if ! min_x :
				min_x = pos.x
			elif min_x > pos.x : 
				min_x = pos.x
			
			if ! min_y : 
				min_y = pos.y
			elif min_y > pos.y : 
				min_y = pos.y

			if ! max_x : 
				max_x = pos.x
			elif max_x < pos.x : 
				max_x = pos.x
			
			if ! max_y : 
				max_y = pos.y
			elif max_y < pos.y : 
				max_y = pos.y

	if !min_x : min_x=0
	if !max_x : max_x=0
	if !min_y : min_y=0
	if !max_y : max_y=0
	
	return Vector2(min_x+(max_x-min_x)/2,min_y+(max_y-min_y)/2)

func calculate_center_of_control(obj:Control):
	return calculate_center(obj.position,obj.size)
	
func calculate_center(position:Vector2,box:Vector2):
	var pos_x=position.x+box.x/2
	var pos_y=position.y+box.x/2
	return Vector2(pos_x,pos_y)

func add_point_to_line(line:LineContainer,point:Vector2):
	editor.add_point_to_line(ref_uuid,line,point)
	


class LinePoint extends Control:
	
	var center_point:Vector2
	var is_first_main_point=false
	var is_last_main_point=false
	var is_inside_main_point=false
	var is_inside_loop_line=false
	var is_state_point=false
	var force_invisibility=false
	var source_line_point:String
	var target_line_point:String
	var transition_ref_uuid:String=""
	var point_radius=8
	var is_choice_point=false
	var choice_type:String=""
	var choice_type_label_panel_container:PanelContainer
	var choice_type_label:CustomEditorLabel
	var editor:StateMachineEditor
	

	func dispose():
		#SMDebugTools.print("dispose LinePoint "+str(self),"dispose")
		if editor == null :
			SMDebugTools.print("!! Already disposed "+str(self))
		editor = null
		

	func _exit_tree():
		dispose()	
	
	func _init():
		choice_type_label_panel_container=PanelContainer.new()
		choice_type_label=CustomEditorLabel.new()
		z_index=StateMachineEditor.GraphicalZIndex.TRANSITION_POINT_Z_INDEX
		size=Vector2(point_radius*2,point_radius*2)
		var sb:StyleBoxFlat=StyleBoxFlat.new()		
		sb.content_margin_top=0
		sb.content_margin_bottom=0
		sb.content_margin_left=0
		sb.content_margin_right=0
		choice_type_label_panel_container.visible=false
		choice_type_label_panel_container.add_theme_stylebox_override("panel",sb)		
		choice_type_label_panel_container.add_child(choice_type_label)
		add_child(choice_type_label_panel_container)
	
	func set_point_position(pos:Vector2):
		center_point=pos
		position=pos-Vector2(point_radius,point_radius)

	func _draw():
		var center = Vector2(point_radius,point_radius)
		var radius = point_radius
		var angle_from = 0
		var angle_to = 360
		var color = points_color

		if editor != null and editor.is_editable :
			color = points_color_edit_mode

		draw_circle(center,radius,color)		

		if editor != null :
			var style_box=choice_type_label_panel_container.get_theme_stylebox("panel")
			if style_box != null :
				style_box.bg_color=editor.get_background_color()
			

		choice_type_label_panel_container.visible=is_choice_point
		if UtilsStatic.is_string_not_null_or_empty(choice_type):
			choice_type_label.text=choice_type
			if choice_type == "OR" :
				choice_type_label_panel_container.position=Vector2(-3,-30)		
			elif choice_type == "AND" :
				choice_type_label_panel_container.position=Vector2(-10,-30)		
		visible=is_choice_point || (editor != null and editor.is_editable)
		if is_inside_loop_line :
			visible = false
		if is_state_point :
			visible = false
		
		if force_invisibility == true :
			visible = false
	

	func _get_drag_data(at_position: Vector2) -> Variant :
		var can_drag=false

		if is_first_main_point or is_last_main_point or is_inside_main_point or UtilsStatic.is_string_not_null_or_empty(source_line_point) or UtilsStatic.is_string_not_null_or_empty(target_line_point) :
			can_drag=true

		if can_drag and editor != null and editor.is_editable :
			var prev:LinePoint=self.duplicate()
			prev.scale=prev.scale*editor.zoom
			set_drag_preview(prev)
			return self
		return null
	
	func _gui_input(event: InputEvent):
		if event is InputEventMouseButton and event.double_click and event.button_index==MOUSE_BUTTON_LEFT :			
			if editor != null and editor.is_editable:								
				editor.remove_point_from_line(self)

class LineContainer extends Control:
	
	var zone_width=7
	var line:Line2D
	var arrow:Arrow
	var source_point:LinePoint
	var target_point:LinePoint
	var graphical_transition:GraphicalTransition	
	var multi_line:MultiLine
	var editor:StateMachineEditor
	var show_arrow=true
	var transition_ref_uuid:String

	func dispose():
		#SMDebugTools.print("dispose LineContainer "+str(self),"dispose")		
		if editor == null :
			SMDebugTools.print("!! Already disposed "+str(self))
		graphical_transition=null
		source_point=null
		target_point=null
		line=null
		multi_line=null
		editor=null
		
	
	func _exit_tree():
		dispose()

	func _init():
		line=Line2D.new()
		arrow=Arrow.new()
		z_index=StateMachineEditor.GraphicalZIndex.TRANSITION_LINK_Z_INDEX
		pivot_offset=Vector2(0,zone_width/2)
		add_child(line)
		line.antialiased=true
		line.width=5
		line.default_color=links_color
		add_child(arrow)
	
	func _ready():
		pass

	func _gui_input(event: InputEvent):
		if event is InputEventMouseButton and event.double_click and event.button_index==MOUSE_BUTTON_LEFT :			
			if get_parent()!=null and get_parent() is CanvasItem and editor != null and editor.is_editable and multi_line != null and multi_line.is_loop_line == false :				
				var point_position=get_parent().get_local_mouse_position() 
				graphical_transition.add_point_to_line(self,point_position)
				
			

	func draw_line_container():
		if source_point and target_point :
			position=source_point.center_point-Vector2(0,zone_width/2)
			var distance=source_point.center_point.distance_to(target_point.center_point)
			var new_angle=source_point.center_point.angle_to_point(target_point.center_point)
			size=Vector2(distance,zone_width)
			rotation=new_angle
			
			var line_points:PackedVector2Array=[Vector2(0,zone_width/2),Vector2(distance,zone_width/2)]
			if multi_line.is_main_line :
				line.default_color=links_color
			else :
				line.default_color=links_color
			line.points=line_points

			arrow.position=Vector2(distance/2-arrow.height/2,zone_width/2)
			arrow.visible = show_arrow
			
			

class MultiLine:
	var line_container_array:Array[LineContainer]=[]
	var points:Array[LinePoint]=[]
	var is_main_line:bool=false
	var is_loop_line:bool=false
	var source_line_state_uuid:String
	var target_line_state_uuid:String
	var labels_box:LabelsBox
	var transition_ref_uuid:String
	var event_name:String
	var editor:StateMachineEditor

	func dispose() :
		# SMDebugTools.print("dispose MultiLine "+str(self))
		if editor == null :
			SMDebugTools.print("!! Already disposed "+str(self))
		for o in line_container_array:			
			o.queue_free()

		line_container_array.clear()
		points.clear()
		if labels_box != null :
			labels_box.queue_free()			
		labels_box=null
		editor=null
		

	func _init():
		labels_box=LabelsBox.new()

	func draw_lines(sme_transition_data:SMETransitionData,sm_transition_data:SMTransition,sm_event_data:SMEvent,graphical_transition:GraphicalTransition):
		var first_point:LinePoint=null
		var last_point:LinePoint=null
		var previous_point:LinePoint=null
		for p in points :
			if first_point==null :
				first_point=p

			if previous_point :
				var line_container:LineContainer=LineContainer.new()
				line_container.transition_ref_uuid=transition_ref_uuid
				line_container.editor=editor
				line_container.graphical_transition=graphical_transition
				line_container.multi_line=self
				line_container_array.append(line_container)
				graphical_transition.add_shape_to_editor(line_container)	
				line_container.source_point=previous_point
				line_container.target_point=p
				line_container.draw_line_container()
			previous_point=p

			last_point=p
	
		if is_main_line==true and first_point !=null and last_point != null :
			var middle_point:Vector2=first_point.center_point+(last_point.center_point-first_point.center_point)/2
			labels_box.editor=editor
			labels_box.transition_ref_uuid=transition_ref_uuid
			labels_box.label.text=event_name
			labels_box.initial_center_position=middle_point
			labels_box.position=middle_point+sme_transition_data.transition_label_offset

			if sm_transition_data.fcts_names != null and sm_transition_data.fcts_names.is_empty() == false :
				for f in sm_transition_data.fcts_names :
					var label_tmp=CustomEditorLabel.new()										
					label_tmp.text="→  "+f
					labels_box.function_panel.functions_box.add_child(label_tmp)												

				labels_box.function_panel.visible=true
			
			if sm_event_data != null and sm_event_data.fcts_names != null and sm_event_data.fcts_names.is_empty() == false :
				for f in sm_event_data.fcts_names :
					var label_tmp=CustomEditorLabel.new()										
					label_tmp.text="→  "+f
					labels_box.event_function_panel.functions_box.add_child(label_tmp)												

				labels_box.event_function_panel.visible=true


			graphical_transition.add_shape_to_editor(labels_box)	

class Arrow extends Control :

	var height = 15	
	var width = 20
	var color=arrows_color
	func _draw():		

		var points = PackedVector2Array()
		points.push_back(Vector2(0,-width/2))
		points.push_back(Vector2(0,width/2))
		points.push_back(Vector2(height,0))		
		var colors = PackedColorArray([color])
		draw_polygon(points, colors)

class LabelsBox extends PanelContainer :

	var vertical_box:VBoxContainer
	var function_panel:FunctionsPanel
	var event_function_panel:FunctionsPanel
	var label:CustomEditorLabel
	var transition_ref_uuid:String
	var initial_center_position:Vector2
	var editor:StateMachineEditor

	func dispose():
		#SMDebugTools.print("dispose LabelsBox "+str(self),"dispose")
		if editor == null :
			SMDebugTools.print("!! Already disposed "+str(self))
		editor = null
		

	func _exit_tree():
		dispose()

	func _init():
		vertical_box=VBoxContainer.new()
		function_panel=FunctionsPanel.new()
		event_function_panel=FunctionsPanel.new()
		label=CustomEditorLabel.new()
		z_index=StateMachineEditor.GraphicalZIndex.TRANSITION_LABEL_Z_INDEX
		#var h_margin=12
		#var v_margin=5
		var h_margin=0
		var v_margin=0
		var sb:StyleBoxFlat=StyleBoxFlat.new()
		sb.bg_color=Color(0.65, 0.18, 0.09,1)
		sb.content_margin_top=v_margin
		sb.content_margin_bottom=v_margin
		sb.content_margin_left=h_margin
		sb.content_margin_right=h_margin
		add_theme_stylebox_override("panel",sb)		
		
		sb=StyleBoxFlat.new()
		sb.bg_color=Color(0, 0, 0,0)
		sb.content_margin_top=4
		sb.content_margin_left=7
		sb.content_margin_right=7
		label.add_theme_stylebox_override("normal",sb)
		vertical_box.add_child(label)
		
		event_function_panel.label.text="evt fcts :"
		event_function_panel.visible=false
		vertical_box.add_child(event_function_panel)

		function_panel.label.text="fcts :"
		function_panel.visible=false
		vertical_box.add_child(function_panel)

		add_child(vertical_box)		

		mouse_entered.connect(_on_mouse_entered)
		mouse_exited.connect(_on_mouse_entexited)
	
	func _on_mouse_entered():
		if editor != null :
			editor.highlight_transition(transition_ref_uuid,true)		

	func _on_mouse_entexited():
		if editor != null :
			editor.highlight_transition(transition_ref_uuid,false)		

	func _get_drag_data(at_position: Vector2) -> Variant :			
		var can_drag=editor != null and editor.is_editable
		
		if can_drag== true :
			var prev:LabelsBox=self.duplicate()
			prev.scale=prev.scale*editor.zoom
			set_drag_preview(prev)
			return self
		return null
	
