@tool
class_name  SMEUpdateListProcessor
extends UpdateListProcessor

func get_uuid(obj)->String:
	if obj is SMEStateData or obj is SMETransitionData :
		return obj.ref_uuid		
	return EditorUtils.get_object_uuid(obj)
