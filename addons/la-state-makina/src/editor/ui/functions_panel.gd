@tool
class_name FunctionsPanel
extends PanelContainer

var h_box:HBoxContainer
var label:CustomEditorLabel
var functions_box:VBoxContainer

func _init():

	h_box=HBoxContainer.new()
	label=CustomEditorLabel.new()
	functions_box=VBoxContainer.new()
	
	var sb:StyleBoxFlat=StyleBoxFlat.new()    
	var corner_radius=0
	sb.content_margin_top=2
	sb.content_margin_bottom=2
	sb.content_margin_left=7
	sb.content_margin_right=7
	sb.bg_color=Color(0, 0, 0,0.5)
	sb.corner_radius_bottom_left=corner_radius
	sb.corner_radius_bottom_right=corner_radius
	sb.corner_radius_top_left=corner_radius
	sb.corner_radius_top_right=corner_radius
	sb.expand_margin_top=0
	sb.expand_margin_bottom=0
	sb.expand_margin_left=0
	sb.expand_margin_right=0
	add_theme_stylebox_override("panel",sb)

	label.size_flags_vertical=SIZE_SHRINK_BEGIN    
	functions_box.add_theme_constant_override("separation",-5)    


	add_child(h_box)
	h_box.add_child(label)
	h_box.add_child(functions_box)