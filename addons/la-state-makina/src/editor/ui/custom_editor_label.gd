@tool
class_name CustomEditorLabel
extends Label


func _init():
	add_theme_font_override("font",SystemFont.new())
	add_theme_font_size_override("font_size",16)
	add_theme_stylebox_override("normal",StyleBoxEmpty.new())
	add_theme_color_override("font_color",Color.WHITE)
