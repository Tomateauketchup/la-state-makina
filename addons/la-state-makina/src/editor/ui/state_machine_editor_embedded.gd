class_name StateMachineEditorEmbedded
extends StateMachineEditor

signal update_json_state_machine(json_path:String)

func set_sme_spec_data(sme_spec:SMESpec):
	var json_file_path=find_json_file_path(data)	
	if FileAccess.file_exists(json_file_path)==false :
		json_file_path=""

	var sm_data:SMStateMachine=null
	var fore_redraw=false
	if UtilsStatic.is_string_not_null_or_empty(json_file_path) :
		var editor_data:StateMachineEditorData = get_editor_data(sme_spec)		
		if editor_data != null:
			var save_on_missing_uuid = true
			if editor_data.state_machine == null :
				editor_data.state_machine = SMStateMachine.new()
			var res=editor_data.state_machine.load_json(json_file_path, save_on_missing_uuid)
			fore_redraw = res["success"] and not res["yet_up_to_date"]
			if ("saving_success" in res) and res["saving_success"]:
				update_json_state_machine.emit(json_file_path) 
			sm_data=editor_data.state_machine

	set_data(sme_spec,sme_spec,sm_data,json_file_path,fore_redraw)


func find_json_file_path(sm:SMESpec)->String:	
	if editor_interface != null :
		var scene_file_path=editor_interface.get_edited_scene_root().scene_file_path	
		var file_name=scene_file_path.get_basename().get_file()
		var directory_path=scene_file_path.get_base_dir()
		return directory_path+"/"+file_name+".json"
	return ""

