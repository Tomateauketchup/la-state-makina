@tool
extends VBoxContainer

enum SMToolsAction {
	NONE,
	GENERATE_SCENE,
	GENERATE_API,
	GENERATE_SCENE_AND_API
};
var action : SMToolsAction

var editor_plugin : EditorPlugin

func register_editor_plugin(_editor_plugin):
	editor_plugin = _editor_plugin

func _on_tree():
	action = SMToolsAction.NONE
	get_node("FileDialog").hide()

func _on_scene_and_api_generator_pressed():
	action = SMToolsAction.GENERATE_SCENE_AND_API
	get_node("FileDialog").show()

func _on_scene_generator_pressed():
	action = SMToolsAction.GENERATE_SCENE
	get_node("FileDialog").show()

func _on_api_generator_pressed():
	action = SMToolsAction.GENERATE_API
	get_node("FileDialog").show()

func path_is_a_json_existing_file(path:String):
	var text : String
	if not path.ends_with(".json"):
		text = "Select a json file."
		printerr(text)
		return false
	if not FileAccess.file_exists(path):
		text = "File %s don't exists !"%path
		printerr(text)
		return false
	return true

func generate_api(path:String) -> bool:
	var text : String
	if not path_is_a_json_existing_file(path):
		return false
	var sm : SMStateMachine = SMStateMachine.new()
	sm.load_json(path)
	if not sm.is_loaded:
		text = "Fail to load the json of the machine state : %s ."%path
		printerr(text)
		return false
	var api_path = path.get_basename() + "_api.gd"
	print(api_path)
	var res : bool
	var update_file = FileAccess.file_exists(api_path)
	res = sm.generate_api_file(api_path)
	if res:
		var fs = editor_plugin.get_editor_interface().get_resource_filesystem()
		if update_file :
			push_warning("TODO : Solve the Reimport BUG in la-state-makina.")
			fs.reimport_files([api_path])
		else:
			fs.update_file(api_path)
	else:
		text = "Failed to generate : " + api_path
		printerr(text)
		return false
	print("Generating State Machine API for : %s success"%path)
	return true
	
func generate_scene(path:String) -> bool :
	if not path_is_a_json_existing_file(path):
		return false
	var _name : String = path.get_file().get_basename()
	var scene_path = path.get_basename() + ".tscn"
	if FileAccess.file_exists(scene_path):
		var text = "The scene %s already exists. We don't regenerate that file."%scene_path
		printerr(text)
		return false
	var packed_scene : PackedScene = PackedScene.new()
	var scene : SMESpec = SMESpec.new()
	scene.name = _name
	packed_scene.pack(scene)
	ResourceSaver.save(packed_scene, scene_path)
	editor_plugin.get_editor_interface().get_resource_filesystem().update_file(
		scene_path
	)
	print("Generating Scene for : %s success"%path)
	return true

func _on_file_dialog_files_selected(paths:PackedStringArray):
	var text : String
	get_node("FileDialog").hide()
	var paths_are_valid = true
	for path in paths:
		if path.get_extension() != "json":
			text = "Invalid path name : %s. Path should be a json file."%path
			printerr(text)
			paths_are_valid = false
	if paths_are_valid:
		if action == SMToolsAction.GENERATE_API:
			for path in paths:
				generate_api(path)
		elif action == SMToolsAction.GENERATE_SCENE:
			for path in paths:
				generate_scene(path)
		elif action == SMToolsAction.GENERATE_SCENE_AND_API:
			for path in paths:
				generate_api(path)
				generate_scene(path)
	else:
		text = "Aborting State Machine Generation. No files were modified."
		printerr(text)
	action = SMToolsAction.NONE
