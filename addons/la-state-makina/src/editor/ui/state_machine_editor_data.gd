@tool
class_name StateMachineEditorData
extends RefCounted

var state_machine:SMStateMachine=SMStateMachine.new()
var is_editable:bool=false
var zoom:float
var scroll_offset:Vector2

func dispose():
	# SMDebugTools.print("dispose StateMachineEditorData "+str(self))
	state_machine=null
