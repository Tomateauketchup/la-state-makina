@tool
class_name GraphicalState
extends GraphNode

var editor:StateMachineEditor
var ref_uuid:String
var fcts_in_label:FunctionsPanel
var fcts_activation_label:FunctionsPanel
var fcts_out_label:FunctionsPanel
var fcts_deactivation_label:FunctionsPanel
var fcts_micro_update_label:FunctionsPanel
var fcts_update_label:FunctionsPanel
var is_initial:bool

func dispose():
	# SMDebugTools.print("dispose GraphicalState "+str(self))
	if editor == null :
		SMDebugTools.print("!! Already disposed "+str(self))
	editor=null

func _exit_tree():
	dispose()

func _init():
	is_initial = false
	fcts_in_label=FunctionsPanel.new()
	fcts_activation_label=FunctionsPanel.new()
	fcts_out_label=FunctionsPanel.new()
	fcts_deactivation_label=FunctionsPanel.new()
	fcts_micro_update_label=FunctionsPanel.new()
	fcts_update_label=FunctionsPanel.new()

	#show_close=true	
	add_theme_color_override("title_color", Color(1, 1, 1,1))	
	add_theme_constant_override("title_h_offset", 15)	
	add_theme_constant_override("title_h_offset", 0)	
	add_theme_constant_override("title_offset", 27)	
	add_theme_constant_override("separation", 4)	
	#add_theme_font_override("title_font",SystemFont.new())
	#add_theme_font_size_override("title_size",20)	
	
	setBoxStyle(false)

	#remove_theme_stylebox_override("frame")
	#add_theme_stylebox_override("frame",sb)
	#add_theme_stylebox_override("selected_frame",sb_selected)
	#add_theme_stylebox_override("comment",StyleBoxEmpty.new())
	#add_theme_stylebox_override("comment_focus",StyleBoxEmpty.new())
	#add_theme_stylebox_override("position",StyleBoxEmpty.new())
	#add_theme_stylebox_override("slot",StyleBoxEmpty.new())
	#add_theme_stylebox_override("breakpoint",StyleBoxEmpty.new())
	
	
	#theme.title_color=Color(1,1,1,1)
	z_index=StateMachineEditor.GraphicalZIndex.STATE_NODE_Z_INDEX
	fcts_in_label.visible=false	
	fcts_activation_label.visible=false
	fcts_out_label.visible=false
	fcts_deactivation_label.visible=false
	fcts_micro_update_label.visible=false
	fcts_update_label.visible=false

	add_child(fcts_in_label)	
	add_child(fcts_activation_label)
	add_child(fcts_out_label)
	add_child(fcts_deactivation_label)
	add_child(fcts_micro_update_label)
	add_child(fcts_update_label)

func _ready():	

	dragged.connect(_on_dragged)
	node_selected.connect(_on_node_selected)
	node_deselected.connect(_on_node_deselected)	

func setBoxStyle(is_active_state:bool):

	var background_color=Color(0.32,0.59,0.38,1)
	var background_color_selected=Color(0.35,0.68,0.9,1)

	if is_active_state == true :
		background_color=Color(1,0,0,1)
		background_color_selected=Color(1,0,0,1)

	var sb:StyleBoxFlat=StyleBoxFlat.new()
	#var h_margin=12
	var h_margin=5
	var corner_radius=0

	if is_initial:
		var border_size : int = 5
		corner_radius = 10
		sb.border_color=Color.PINK
		sb.border_width_bottom=border_size
		sb.border_width_top=border_size
		sb.border_width_right=border_size
		sb.border_width_left=border_size
	
	sb.content_margin_top=35
	sb.content_margin_bottom=h_margin
	
	sb.content_margin_left=h_margin
	sb.content_margin_right=h_margin
	sb.bg_color=background_color
	sb.corner_radius_bottom_left=corner_radius
	sb.corner_radius_bottom_right=corner_radius
	sb.corner_radius_top_left=corner_radius
	sb.corner_radius_top_right=corner_radius
	sb.expand_margin_top=0
	sb.expand_margin_bottom=0
	sb.expand_margin_left=0
	sb.expand_margin_right=0

	var sb_selected=sb.duplicate()
	sb_selected.bg_color=background_color_selected

	#remove_theme_stylebox_override("frame")
	add_theme_stylebox_override("panel",sb)
	add_theme_stylebox_override("panel_selected",sb_selected)


func update_data(
	sme_data:SMEStateData, sm_state_data:SMState, sm_machine_data:SMStateMachine
):
	if sme_data != null and sm_state_data != null :
		position_offset=sme_data.position
		title=sme_data.name
		
		is_initial = sm_machine_data.is_initial(sme_data.name)
		update_fcts_label(fcts_in_label,"fcts_in",sm_state_data.fcts_in_names)
		update_fcts_label(fcts_activation_label,"fcts_activation",sm_state_data.fcts_activation_names)
		update_fcts_label(fcts_out_label,"fcts_out",sm_state_data.fcts_out_names)
		update_fcts_label(fcts_deactivation_label,"fcts_deactivation",sm_state_data.fcts_deactivation_names)
		update_fcts_label(fcts_micro_update_label,"fcts_micro_update",sm_state_data.fcts_micro_update_names)
		update_fcts_label(fcts_update_label,"fcts_update",sm_state_data.fcts_update_names)
		setBoxStyle(sm_state_data.is_active)

func update_fcts_label(label:FunctionsPanel,label_name:String,fcts_names:Array[String]):
	label.label.text=label_name+" :"
	for c in label.functions_box.get_children() :
		c.queue_free()		
	
	if fcts_names != null && fcts_names.is_empty() == false :
		for n in fcts_names :
			var lab=CustomEditorLabel.new()			
			lab.text="→  "+n			
			label.functions_box.add_child(lab)		
		label.visible=true
	else :
		label.visible=false


func _on_dragged(from: Vector2, to: Vector2):
	if from != to:
		editor.do_undo_redo_state_position_changed(ref_uuid,to,from)
	
func _on_node_selected():
	editor.node_selected(ref_uuid)

func _on_node_deselected():
	editor.node_deselected(ref_uuid)
	
