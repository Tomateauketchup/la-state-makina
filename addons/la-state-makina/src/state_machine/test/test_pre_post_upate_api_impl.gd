extends "res://addons/la-state-makina/src/state_machine/test/test_pre_post_upate_api.gd"

var pre_start_flag : float
var post_start_flag : float
var pre_update_flag : float
var post_update_flag : float

func _init():
	pre_start_flag = 0
	post_start_flag = 0
	pre_update_flag = 0
	post_update_flag = 0

func ef_pre_start(_sm:SMStateMachine):
	pre_start_flag = _sm.get_time()

func ef_post_start(_sm:SMStateMachine):
	post_start_flag = _sm.get_time()

func ef_pre_update(_sm:SMStateMachine, _delta:float):
	pre_update_flag = _sm.get_time()
	assert( (0.0 < _delta) and (_delta < 1.0) )

func ef_post_update(_sm:SMStateMachine, _delta:float):
	post_update_flag = _sm.get_time()
	assert( (0.0 < _delta) and (_delta < 1.0) )


