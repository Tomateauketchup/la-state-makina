class_name UnitTestSMStateMachine
extends GdUnitTestSuite

var SMApi
var test_path : String = "res://addons/la-state-makina/src/state_machine/test"

var test_user_directory : String = "user://tests"
var test_user_sm_path : String = test_user_directory + "/state_machine"

func before():
	if not DirAccess.dir_exists_absolute(test_user_directory) :
		DirAccess.make_dir_absolute(test_user_directory)
	if not DirAccess.dir_exists_absolute(test_user_sm_path) :
		DirAccess.make_dir_absolute(test_user_sm_path)
	SMApi = load(test_path+"/sm_test_api_imp.gd")

func after():
	pass

func files_are_identic(path_1, path_2) -> bool:
	var file_1 = FileAccess.open(path_1, FileAccess.READ)
	assert_bool(file_1 != null).is_true()
	var content_1 = file_1.get_as_text()
	var file_2 = FileAccess.open(path_2, FileAccess.READ)
	assert_bool(file_2 != null).is_true()
	var content_2 = file_2.get_as_text()
	return content_1 == content_2

func test_generate_api_code():
	var state_machine : SMStateMachine = SMStateMachine.new()
	state_machine.load_json(test_path+"/music_player.json")
	state_machine.generate_api_file(test_user_sm_path+"/music_player_sm_api.gd")
	assert_file(test_user_sm_path+"/music_player_sm_api.gd").exists()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/music_player_sm_api.gd",
			test_path+"/music_player_sm_api.gd" 
		)
	).is_true()
	
	state_machine = SMStateMachine.new()
	state_machine.load_json(test_path+"/sm_test_1.json")
	state_machine.generate_api_file(test_user_sm_path+"/sm_test_1_api.gd")
	assert_file(test_user_sm_path+"/sm_test_1_api.gd").exists()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/sm_test_1_api.gd",
			test_path+"/sm_test_1_api.gd" 
		)
	).is_true()

class Class_test extends "res://addons/la-state-makina/src/state_machine/test/test_api_extends_api.gd":
	func _init():
		pass

func test_generate_api_code_with_extends_class():
	var state_machine : SMStateMachine = SMStateMachine.new()
	state_machine.load_json(test_path+"/test_api_extends.json")
	state_machine.generate_api_file(test_user_sm_path+"/test_api_extends_api.gd")
	assert_file(test_user_sm_path+"/test_api_extends_api.gd").exists()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/test_api_extends_api.gd",
			test_path+"/test_api_extends_api.gd" 
		)
	).is_true()
	
	state_machine = SMStateMachine.new()
	state_machine.load_json(test_path+"/sm_test_1.json")
	state_machine.generate_api_file(test_user_sm_path+"/sm_test_1_api.gd")
	assert_file(test_user_sm_path+"/sm_test_1_api.gd").exists()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/sm_test_1_api.gd",
			test_path+"/sm_test_1_api.gd" 
		)
	).is_true()
	
	var res = Class_test.new()
	assert_int(res.parent_value).is_equal(42)


func test_load_json():
	var sm_api = load(test_path+"/music_player_sm_api.gd").new()
	var state_machine : SMStateMachine = SMStateMachine.new()
	var res : Dictionary = state_machine.load_json(test_path+"/music_player.json")
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	state_machine.register_api(sm_api)
	assert_str( str(state_machine.states) ).is_equal(
		"[WAITING_END_OF_INITIALIZATION, MUSIC_IS_PLAYING, WAITING_A_PLAY_ORDER, URGENCY_STOP]"
	)
	assert_str( str(state_machine.events) ).is_equal(
		"[INITIALIZATION_IS_FINISHED, USER_ASK_TO_PLAY, USER_ASK_TO_STOP, EMERGENCY_OCCURS, USER_ASK_TO_RESTART]"
	)
	assert_str( str(state_machine.transitions) ).is_equal(
		"[(WAITING_END_OF_INITIALIZATION, MUSIC_IS_PLAYING, WAITING_A_PLAY_ORDER, URGENCY_STOP, -> URGENCY_STOP,  : EMERGENCY_OCCURS - t:or), (WAITING_END_OF_INITIALIZATION, -> WAITING_A_PLAY_ORDER,  : INITIALIZATION_IS_FINISHED - t:and), (WAITING_A_PLAY_ORDER, -> MUSIC_IS_PLAYING,  : USER_ASK_TO_PLAY - t:and), (MUSIC_IS_PLAYING, -> WAITING_A_PLAY_ORDER,  : USER_ASK_TO_STOP - t:and), (MUSIC_IS_PLAYING, WAITING_A_PLAY_ORDER, -> WAITING_END_OF_INITIALIZATION,  : USER_ASK_TO_RESTART - t:and)]"
	)
	assert_str( str(state_machine.init_state_names()) ).is_equal(
		'[&"WAITING_END_OF_INITIALIZATION"]'
	)

	# Try to load the same machine state again
	res = state_machine.load_json(test_path+"/music_player.json")
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_true()

func test_load_json_with_saving_uuid():
	var error = DirAccess.copy_absolute(
		test_path+"/sm_with_missing_uuid.json", test_user_sm_path+"/sm_with_missing_uuid.json"
	)
	assert_bool(error == OK).is_true()
	assert_file(test_user_sm_path+"/sm_with_missing_uuid.json").exists()
	
	var state_machine : SMStateMachine = SMStateMachine.new()
	var save_on_missing_uuid = false
	var res : Dictionary = state_machine.load_json(
		test_user_sm_path+"/sm_with_missing_uuid.json",
		save_on_missing_uuid
	)
	assert_bool(res.get("success")).is_true()
	assert_bool(res.get("yet_up_to_date")).is_false()
	assert_bool("saving_success" in res).is_false()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/sm_with_missing_uuid.json",
			test_path+"/sm_with_missing_uuid.json"
		)
	).is_true()
	res = state_machine.load_json(
		test_user_sm_path+"/sm_with_missing_uuid.json",
		save_on_missing_uuid
	)
	assert_bool(res.get("success")).is_true()
	assert_bool(res.get("yet_up_to_date")).is_true()
	assert_bool("saving_success" in res).is_false()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/sm_with_missing_uuid.json",
			test_path+"/sm_with_missing_uuid.json"
		)
	).is_true()

	state_machine = SMStateMachine.new()
	save_on_missing_uuid = true
	res = state_machine.load_json(
		test_user_sm_path+"/sm_with_missing_uuid.json",
		save_on_missing_uuid
	)
	assert_bool(res.get("success")).is_true()
	assert_bool(res.get("yet_up_to_date")).is_false()
	assert_bool(res.get("saving_success")).is_true()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/sm_with_missing_uuid.json",
			test_path+"/sm_with_missing_uuid.json"
		)
	).is_false()
	res = state_machine.load_json(
		test_user_sm_path+"/sm_with_missing_uuid.json",
		save_on_missing_uuid
	)
	assert_bool(res.get("success")).is_true()
	assert_bool(res.get("yet_up_to_date")).is_true()
	assert_bool("saving_success" in res ).is_false()

func test_generate_json():
	# var sm_api = load(test_path+"/music_player_sm_api.gd").new()
	var state_machine : SMStateMachine = SMStateMachine.new()
	state_machine.load_json(test_path+"/music_player.json")
	state_machine.generate_json_file(test_user_sm_path+"/test_music_player.json")
	assert_file(test_user_sm_path+"/test_music_player.json").exists()
	assert_bool(
		files_are_identic(
			test_user_sm_path+"/test_music_player.json",
			test_path+"/music_player.json"
		)
	).is_true()


func test_sm_1():
	var evt_fct = func(_ms: SMStateMachine, _ev:int, mu:int):
		if mu == 0:
			return true
		return false
	
	var api = SMApi.new()
	api.register_evt_fct(Callable(evt_fct))
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/sm_test_1.json")
	
	var res : Array[String] = [""]
	
	var clear = func():
		res[0] = ""

	var print_debug_flag = false
	
	var add_text  = func(text):
		if print_debug_flag:
			print(text)
		res[0]+= text + "\n"

	var started_callback = func(_ms:SMStateMachine, active_states:Array[int]):
		add_text.call("started " + str(active_states))
		
	var update_finished_callback = func(_ms:SMStateMachine, active_states:Array[int], crossed_trans:Dictionary):
		var tab = crossed_trans.keys()
		tab.sort()
		add_text.call("update_finished " + str(active_states) + " - " + str(tab))
	
	var micro_update_finished_callback = func(_ms:SMStateMachine, active_states:Array[int], crossed_trans:Array[int], cpt:int):
		add_text.call("micro_update_finished " + str(active_states) + " - " + str(crossed_trans) + " - " + str(cpt) )
	
	sm.connect("started", started_callback)
	sm.connect("micro_update_finished", micro_update_finished_callback)
	sm.connect("update_finished", update_finished_callback)
	
	sm.register_api(api)

	api.clear()
	clear.call()
	sm.start()
	assert_str(api.result).is_equal(
		"activation 0 - [] - -1\n" +
		"activation 1 - [] - -1\n" +
		"in 0 - [] - -1\n" +
		"in 1 - [] - -1\n"
	)
	assert_array(sm.active_states).is_equal([0, 1])
	assert_str(res[0]).is_equal("started [0, 1]\n")
	
	assert_bool(sm.states[0].is_active).is_true()
	assert_bool(sm.states[0].rising_edge_on_update()).is_true()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_true()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_false()
	assert_bool(sm.states[2].rising_edge_on_update()).is_false()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()
	
	api.clear()
	clear.call()
	sm.update()
	assert_array(sm.active_states).is_equal([1, 2])
	assert_str(api.result).is_equal(
		"micro_update 0 - 0\n" +
		"micro_update 1 - 0\n" +
		"test evt 0 - 0\n" +
		"out 1 - [1] - 0\n" +
		"out 0 - [0] - 0\n" +
		"deactivation 0 - [0] - 0\n" +
		"trans 0 - 0\n" +
		"trans 1 - 0\n" +
		"activation 2 - [1] - 0\n" +
		"in 1 - [0] - 0\n" +
		"in 2 - [1] - 0\n" +
		"micro_update 1 - 1\n" +
		"micro_update 2 - 1\n" +
		"test evt 0 - 1\n" +
		"update 1\n" +
		"update 2\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [1, 2] - [0, 1] - 0\n" +
		"micro_update_finished [1, 2] - [] - 1\n" + 
		"update_finished [1, 2] - [0, 1]\n"
	)
	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_true()
	
	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_true()
	assert_bool(sm.states[2].rising_edge_on_update()).is_true()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()
	
	api.clear()
	clear.call()
	sm.update()
	assert_str(api.result).is_equal(
		"micro_update 1 - 0\n" +
		"micro_update 2 - 0\n" +
		"test evt 0 - 0\n" +
		"out 1 - [1] - 0\n" +
		"deactivation 1 - [1] - 0\n" +
		"trans 1 - 0\n" +
		"in 2 - [1] - 0\n" +
		"micro_update 2 - 1\n" +
		"update 2\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [2] - [1] - 0\n" +
		"micro_update_finished [2] - [] - 1\n" + 
		"update_finished [2] - [1]\n"
	)
	assert_array(sm.active_states).is_equal([2])
	
	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_false()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_true()

	assert_bool(sm.states[2].is_active).is_true()
	assert_bool(sm.states[2].rising_edge_on_update()).is_false()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()
	assert_str(api.result).is_equal(
		"micro_update 2 - 0\n" + 
		"update 2\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [2] - [] - 0\n" +
		"update_finished [2] - []\n"
	)
	assert_array(sm.active_states).is_equal([2])
	
	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_false()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_true()
	assert_bool(sm.states[2].rising_edge_on_update()).is_false()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()
	assert_str(api.result).is_equal(
		"micro_update 2 - 0\n" + 
		"update 2\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [2] - [] - 0\n" +
		"update_finished [2] - []\n"
	)
	assert_array(sm.active_states).is_equal([2])
	
	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_false()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_true()
	assert_bool(sm.states[2].rising_edge_on_update()).is_false()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()


func test_sm_2():
	var evt_fct = func(_ms: SMStateMachine, _ev:int, mu:int):
		if mu == 0:
			return true
		return false
	
	var api = SMApi.new()
	api.register_evt_fct(Callable(evt_fct))
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/sm_test_2.json")
	
	var res : Array[String] = [""]	
		
	var clear = func():
		res[0] = ""

	api.print_debug_flag = false
	var print_debug_flag = false
	
	var add_text  = func(text):
		if print_debug_flag:
			print(text)
		res[0]+= text + "\n"

	var started_callback = func(_ms:SMStateMachine, active_states:Array[int]):
		add_text.call("started " + str(active_states))
		
	var update_finished_callback = func(_ms:SMStateMachine, active_states:Array[int], crossed_trans:Dictionary):
		var tab = crossed_trans.keys()
		tab.sort()
		add_text.call("update_finished " + str(active_states) + " - " + str(tab))
	
	var micro_update_finished_callback = func(_ms:SMStateMachine, active_states:Array[int], crossed_trans:Array[int], cpt:int):
		add_text.call("micro_update_finished " + str(active_states) + " - " + str(crossed_trans) + " - " + str(cpt) )
	
	sm.connect("started", started_callback)
	sm.connect("micro_update_finished", micro_update_finished_callback)
	sm.connect("update_finished", update_finished_callback)
	
	sm.register_api(api)

	api.clear()
	clear.call()
	sm.start()
	assert_str(api.result).is_equal(
		"activation 0 - [] - -1\n" +
		"activation 2 - [] - -1\n" +
		"in 0 - [] - -1\n" +
		"in 2 - [] - -1\n"
	)

	assert_array(sm.active_states).is_equal([0, 2])
	assert_str(res[0]).is_equal("started [0, 2]\n")
	
	for i in range(len(sm.states)):
		if not i in [0, 2]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[0].is_active).is_true()
	assert_bool(sm.states[0].rising_edge_on_update()).is_true()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_true()
	assert_bool(sm.states[2].rising_edge_on_update()).is_true()
	assert_bool(sm.states[2].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()
	
	assert_array(sm.active_states).is_equal([1, 3])
	assert_str(api.result).is_equal(
		"micro_update 0 - 0\n" +
		"micro_update 2 - 0\n" +
		"test evt 0 - 0\n" +
		"out 2 - [1] - 0\n" +
		"out 0 - [0] - 0\n" +
		"deactivation 2 - [1] - 0\n" +
		"deactivation 0 - [0] - 0\n" +
		"trans 0 - 0\n" +
		"trans 1 - 0\n" +
		"activation 1 - [0] - 0\n" +
		"activation 3 - [1] - 0\n" +
		"in 1 - [0] - 0\n" +
		"in 3 - [1] - 0\n" +
		"micro_update 1 - 1\n" +
		"micro_update 3 - 1\n" +
		"test evt 0 - 1\n" +
		"update 1\n" +
		"update 3\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [1, 3] - [0, 1] - 0\n" +
		"micro_update_finished [1, 3] - [] - 1\n" +
		"update_finished [1, 3] - [0, 1]\n"
	)
	for i in range(len(sm.states)):
		if not i in [0, 2, 1, 3]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_true()

	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_true()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[2].is_active).is_false()
	assert_bool(sm.states[2].rising_edge_on_update()).is_false()
	assert_bool(sm.states[2].falling_edge_on_update()).is_true()

	assert_bool(sm.states[3].is_active).is_true()
	assert_bool(sm.states[3].rising_edge_on_update()).is_true()
	assert_bool(sm.states[3].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 4])
	assert_str(api.result).is_equal(
		"micro_update 1 - 0\n" +
		"micro_update 3 - 0\n" +
		"test evt 0 - 0\n" +
		"out 3 - [2] - 0\n" +
		"deactivation 3 - [2] - 0\n" +
		"trans 2 - 0\n" +
		"activation 4 - [2] - 0\n" +
		"in 4 - [2] - 0\n" +
		"micro_update 1 - 1\n" +
		"micro_update 4 - 1\n" +
		"test evt 0 - 1\n" +
		"update 1\n" +
		"update 4\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [1, 4] - [2] - 0\n" +
		"micro_update_finished [1, 4] - [] - 1\n" +
		"update_finished [1, 4] - [2]\n"
	)
	for i in range(len(sm.states)):
		if not i in [1, 3, 1, 4]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[3].is_active).is_false()
	assert_bool(sm.states[3].rising_edge_on_update()).is_false()
	assert_bool(sm.states[3].falling_edge_on_update()).is_true()

	assert_bool(sm.states[4].is_active).is_true()
	assert_bool(sm.states[4].rising_edge_on_update()).is_true()
	assert_bool(sm.states[4].falling_edge_on_update()).is_false()
	
	api.clear()
	clear.call()
	sm.update()
	
	assert_array(sm.active_states).is_equal([7, 8, 9])
	assert_str(api.result).is_equal(
		"micro_update 1 - 0\n" +
		"micro_update 4 - 0\n" +
		"test evt 0 - 0\n" +
		"out 4 - [4, 5] - 0\n" +
		"out 1 - [4] - 0\n" +
		"deactivation 4 - [4, 5] - 0\n" +
		"deactivation 1 - [4] - 0\n" +
		"trans 4 - 0\n" +
		"trans 5 - 0\n" +
		"activation 7 - [4] - 0\n" +
		"activation 8 - [4, 5] - 0\n" +
		"activation 9 - [5] - 0\n" +
		"in 7 - [4] - 0\n" +
		"in 8 - [4, 5] - 0\n" +
		"in 9 - [5] - 0\n" +
		"micro_update 7 - 1\n" +
		"micro_update 8 - 1\n" +
		"micro_update 9 - 1\n" +
		"test evt 0 - 1\n" +
		"update 7\n" +
		"update 8\n" +
		"update 9\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [7, 8, 9] - [4, 5] - 0\n" +
		"micro_update_finished [7, 8, 9] - [] - 1\n" +
		"update_finished [7, 8, 9] - [4, 5]\n"
	)
	for i in range(len(sm.states)):
		if not i in [1, 4, 7, 8, 9]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[1].is_active).is_false()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_true()

	assert_bool(sm.states[4].is_active).is_false()
	assert_bool(sm.states[4].rising_edge_on_update()).is_false()
	assert_bool(sm.states[4].falling_edge_on_update()).is_true()

	assert_bool(sm.states[7].is_active).is_true()
	assert_bool(sm.states[7].rising_edge_on_update()).is_true()
	assert_bool(sm.states[7].falling_edge_on_update()).is_false()

	assert_bool(sm.states[8].is_active).is_true()
	assert_bool(sm.states[8].rising_edge_on_update()).is_true()
	assert_bool(sm.states[8].falling_edge_on_update()).is_false()

	assert_bool(sm.states[9].is_active).is_true()
	assert_bool(sm.states[9].rising_edge_on_update()).is_true()
	assert_bool(sm.states[9].falling_edge_on_update()).is_false()
	
	api.clear()
	clear.call()
	sm.update()

	assert_array(sm.active_states).is_equal([0, 10, 11])
	assert_str(api.result).is_equal(
		"micro_update 7 - 0\n" +
		"micro_update 8 - 0\n" +
		"micro_update 9 - 0\n" +
		"test evt 0 - 0\n" +
		"out 9 - [7] - 0\n" +
		"out 8 - [9] - 0\n" +
		"out 7 - [6] - 0\n" +
		"deactivation 9 - [7] - 0\n" +
		"deactivation 8 - [9] - 0\n" +
		"deactivation 7 - [6] - 0\n" +
		"trans 6 - 0\n" +
		"trans 7 - 0\n" +
		"trans 9 - 0\n" +
		"activation 0 - [6] - 0\n" +
		"activation 10 - [7] - 0\n" +
		"activation 11 - [9] - 0\n" +
		"in 0 - [6] - 0\n" +
		"in 10 - [7] - 0\n" +
		"in 11 - [9] - 0\n" +
		"micro_update 0 - 1\n" +
		"micro_update 10 - 1\n" +
		"micro_update 11 - 1\n" +
		"test evt 0 - 1\n" +
		"update 0\n" +
		"update 10\n" +
		"update 11\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [0, 10, 11] - [6, 7, 9] - 0\n" +
		"micro_update_finished [0, 10, 11] - [] - 1\n" +
		"update_finished [0, 10, 11] - [6, 7, 9]\n"
	)
	for i in range(len(sm.states)):
		if not i in [7, 8, 9, 0, 10, 11]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[7].is_active).is_false()
	assert_bool(sm.states[7].rising_edge_on_update()).is_false()
	assert_bool(sm.states[7].falling_edge_on_update()).is_true()

	assert_bool(sm.states[8].is_active).is_false()
	assert_bool(sm.states[8].rising_edge_on_update()).is_false()
	assert_bool(sm.states[8].falling_edge_on_update()).is_true()

	assert_bool(sm.states[9].is_active).is_false()
	assert_bool(sm.states[9].rising_edge_on_update()).is_false()
	assert_bool(sm.states[9].falling_edge_on_update()).is_true()

	assert_bool(sm.states[0].is_active).is_true()
	assert_bool(sm.states[0].rising_edge_on_update()).is_true()
	assert_bool(sm.states[0].falling_edge_on_update()).is_false()

	assert_bool(sm.states[10].is_active).is_true()
	assert_bool(sm.states[10].rising_edge_on_update()).is_true()
	assert_bool(sm.states[10].falling_edge_on_update()).is_false()

	assert_bool(sm.states[11].is_active).is_true()
	assert_bool(sm.states[11].rising_edge_on_update()).is_true()
	assert_bool(sm.states[11].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 5, 11])
	assert_str(api.result).is_equal(
		"micro_update 0 - 0\n" +
		"micro_update 10 - 0\n" +
		"micro_update 11 - 0\n" +
		"test evt 0 - 0\n" +
		"out 10 - [8] - 0\n" +
		"out 0 - [0] - 0\n" +
		"deactivation 10 - [8] - 0\n" +
		"deactivation 0 - [0] - 0\n" +
		"trans 0 - 0\n" +
		"trans 8 - 0\n" +
		"activation 1 - [0] - 0\n" +
		"activation 5 - [8] - 0\n" +
		"in 1 - [0] - 0\n" +
		"in 5 - [8] - 0\n" +
		"micro_update 1 - 1\n" +
		"micro_update 5 - 1\n" +
		"micro_update 11 - 1\n" +
		"test evt 0 - 1\n" +
		"update 1\n" +
		"update 5\n" +
		"update 11\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [1, 5, 11] - [0, 8] - 0\n" +
		"micro_update_finished [1, 5, 11] - [] - 1\n" +
		"update_finished [1, 5, 11] - [0, 8]\n"
	)
	
	for i in range(len(sm.states)):
		if not i in [11, 0, 10, 1, 5]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[11].is_active).is_true()
	assert_bool(sm.states[11].rising_edge_on_update()).is_false()
	assert_bool(sm.states[11].falling_edge_on_update()).is_false()

	assert_bool(sm.states[0].is_active).is_false()
	assert_bool(sm.states[0].rising_edge_on_update()).is_false()
	assert_bool(sm.states[0].falling_edge_on_update()).is_true()

	assert_bool(sm.states[10].is_active).is_false()
	assert_bool(sm.states[10].rising_edge_on_update()).is_false()
	assert_bool(sm.states[10].falling_edge_on_update()).is_true()

	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_true()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[5].is_active).is_true()
	assert_bool(sm.states[5].rising_edge_on_update()).is_true()
	assert_bool(sm.states[5].falling_edge_on_update()).is_false()

	api.clear()
	clear.call()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 6, 11])
	
	api.clear()
	clear.call()
	sm.update()

	assert_str(api.result).is_equal(
		"micro_update 1 - 0\n" +
		"micro_update 6 - 0\n" +
		"micro_update 11 - 0\n" +
		"test evt 0 - 0\n" +
		"out 6 - [5] - 0\n" +
		"deactivation 6 - [5] - 0\n" +
		"trans 5 - 0\n" +
		"activation 8 - [5] - 0\n" +
		"activation 9 - [5] - 0\n" +
		"in 8 - [5] - 0\n" +
		"in 9 - [5] - 0\n" +
		"micro_update 1 - 1\n" +
		"micro_update 8 - 1\n" +
		"micro_update 9 - 1\n" +
		"micro_update 11 - 1\n" +
		"test evt 0 - 1\n" +
		"update 1\n" +
		"update 8\n" +
		"update 9\n" +
		"update 11\n"
	)
	assert_str(res[0]).is_equal(
		"micro_update_finished [1, 8, 9, 11] - [5] - 0\n" +
		"micro_update_finished [1, 8, 9, 11] - [] - 1\n" +
		"update_finished [1, 8, 9, 11] - [5]\n"
	)

	assert_array(sm.active_states).is_equal([1, 8, 9, 11])
		
	for i in range(len(sm.states)):
		if not i in [11, 1, 6, 1, 8, 9]:
			assert_bool(sm.states[i].is_active).is_false()
			assert_bool(sm.states[i].rising_edge_on_update()).is_false()
			assert_bool(sm.states[i].falling_edge_on_update()).is_false()
	
	assert_bool(sm.states[11].is_active).is_true()
	assert_bool(sm.states[11].rising_edge_on_update()).is_false()
	assert_bool(sm.states[11].falling_edge_on_update()).is_false()

	assert_bool(sm.states[1].is_active).is_true()
	assert_bool(sm.states[1].rising_edge_on_update()).is_false()
	assert_bool(sm.states[1].falling_edge_on_update()).is_false()

	assert_bool(sm.states[6].is_active).is_false()
	assert_bool(sm.states[6].rising_edge_on_update()).is_false()
	assert_bool(sm.states[6].falling_edge_on_update()).is_true()

	assert_bool(sm.states[8].is_active).is_true()
	assert_bool(sm.states[8].rising_edge_on_update()).is_true()
	assert_bool(sm.states[8].falling_edge_on_update()).is_false()

	assert_bool(sm.states[9].is_active).is_true()
	assert_bool(sm.states[9].rising_edge_on_update()).is_true()
	assert_bool(sm.states[9].falling_edge_on_update()).is_false()

func test_clone():
	var sm_api = load(test_path+"/music_player_sm_api.gd").new()
	var state_machine : SMStateMachine = SMStateMachine.new()
	state_machine.load_json(test_path+"/music_player.json")
	state_machine.register_api(sm_api)
	
	var small_test_clone = func(sm):
		sm.generate_api_file(test_user_sm_path+"/clone_music_player_sm_api.gd")
		assert_file(test_user_sm_path+"/clone_music_player_sm_api.gd").exists()
		assert_bool(
			files_are_identic(
				test_user_sm_path+"/clone_music_player_sm_api.gd",
				test_path+"/music_player_sm_api.gd" 
			)
		).is_true()
		sm.generate_json_file(test_user_sm_path+"/clone_test_music_player.json")
		assert_file(test_user_sm_path+"/clone_test_music_player.json").exists()
		assert_bool(
			files_are_identic(
				test_user_sm_path+"/clone_test_music_player.json",
				test_path+"/music_player.json"
			)
		).is_true()
		
	var clone_sm = state_machine.clone(true, true)
	small_test_clone.call(clone_sm)
	assert_bool(clone_sm.state_to_int.size()!=0).is_true()
	assert_dict(clone_sm.state_to_int).is_equal(state_machine.state_to_int)
	
	clone_sm = state_machine.clone(true, false)
	small_test_clone.call(clone_sm)
	assert_bool(clone_sm.state_to_int.size()!=0).is_true()
	assert_dict(clone_sm.state_to_int).is_equal(state_machine.state_to_int)
	
	clone_sm = state_machine.clone(false, false)
	small_test_clone.call(clone_sm)
	assert_bool(clone_sm.state_to_int.size()!=0).is_true()
	assert_dict(clone_sm.state_to_int).is_equal(state_machine.state_to_int)

func test_stop():
	push_warning("TODO test_stop")

func test_restart():
	push_warning("TODO test_restart")

func test_start():
	push_warning("TODO test_start")

func test_force_init_states():
	push_warning("TODO test_force_init_states")

func test_clock_and_pause():
	push_warning("TODO test_clock_and_pause")

func test_save_load_execution_into_json():
	var evt_fct = func(_ms: SMStateMachine, _ev:int, mu:int):
		if mu == 0:
			return true
		return false
	
	var api = SMApi.new()
	api.register_evt_fct(Callable(evt_fct))
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/sm_test_2.json")
	
	api.print_debug_flag = false

	sm.register_api(api)

	var json_execution_path = test_user_sm_path + "/test_sm_1_execution.json"

	var old_time : int = 0
	var current_time : int = 0
	assert_int(sm.get_current_update_time_us()).is_equal(current_time)
	assert_int(sm.get_old_update_time_us()).is_equal(old_time)

	api.clear()
	sm.start()

	var tmp_state : SMState
	var tmp_id : int = 4

	assert_array(sm.active_states).is_equal([0, 2])
	sm.default_clock.pause()

	assert_bool(sm.get_current_update_time_us()>sm.get_old_update_time_us()).is_true()
	assert_int(sm.get_old_update_time_us()).is_equal(current_time)
	old_time = sm.get_old_update_time_us()
	current_time = sm.get_current_update_time_us()
	assert_bool(sm.get_current_update_time_us()>sm.get_old_update_time_us()).is_true()

	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()

	api.clear()
	sm.update()

	assert_bool(sm.get_current_update_time_us()>sm.get_old_update_time_us()).is_true()
	assert_int(sm.get_old_update_time_us()).is_equal(current_time)
	old_time = sm.get_old_update_time_us()
	current_time = sm.get_current_update_time_us()
	assert_bool(sm.get_current_update_time_us()>sm.get_old_update_time_us()).is_true()

	assert_array(sm.active_states).is_equal([1, 3])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	assert_bool(sm.get_current_update_time_us()>sm.get_old_update_time_us()).is_true()
	assert_int(sm.get_old_update_time_us()).is_equal(0)
	assert_int(sm.get_old_update_time_us()).is_not_equal(old_time)
	assert_int(sm.get_current_update_time_us()).is_not_equal(current_time)

	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	assert_int(sm.get_old_update_time_us()).is_equal(old_time)
	assert_int(sm.get_current_update_time_us()).is_equal(current_time)

	sm.default_clock.resume()

	api.clear()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 4])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()
	
	api.clear()
	sm.update()
	
	assert_array(sm.active_states).is_equal([7, 8, 9])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()
	
	api.clear()
	sm.update()

	assert_array(sm.active_states).is_equal([0, 10, 11])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()

	api.clear()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 5, 11])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()

	api.clear()
	sm.update()

	assert_array(sm.active_states).is_equal([1, 6, 11])
	sm.default_clock.pause()
	tmp_state = sm.states[tmp_id].clone(true, false)
	sm.save_execution_into_json(json_execution_path)
	sm.start()
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.states[tmp_id].has_same_execution_state(tmp_state)).is_true()
	sm.default_clock.resume()

func test_load_another_json():
	var sm = SMStateMachine.new()
	var res = sm.load_json(test_path+"/sm_test_1.json")
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	res = sm.load_json(test_path+"/test_metadata_1.json")
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	assert_dict(sm.state_static_metadata).is_not_empty()
	assert_array(sm.state_static_metadata.keys()).is_equal(sm.state_to_int.values())
	assert_dict(sm.event_static_metadata).is_not_empty()
	assert_array(sm.event_static_metadata.keys()).is_equal(sm.evt_to_int.values())
	assert_dict(sm.get_state_metadata("WAITING_A_PLAY_ORDER")).is_not_empty()

func test_save_static_metadata_into_json():
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/sm_test_1.json")
	assert_dict(sm.state_static_metadata).is_not_empty()
	assert_array(sm.state_static_metadata.keys()).is_equal(sm.state_to_int.values())
	assert_dict(sm.event_static_metadata).is_not_empty()
	assert_array(sm.event_static_metadata.keys()).is_equal(sm.evt_to_int.values())

	sm = SMStateMachine.new()
	sm.load_json(test_path+"/test_metadata_1.json")
	assert_dict(sm.state_static_metadata).is_not_empty()
	assert_array(sm.state_static_metadata.keys()).is_equal(sm.state_to_int.values())
	assert_dict(sm.event_static_metadata).is_not_empty()
	assert_array(sm.event_static_metadata.keys()).is_equal(sm.evt_to_int.values())
	assert_dict(sm.get_state_metadata("WAITING_A_PLAY_ORDER")).is_not_empty()
	assert_vector(
		sm.get_state_metadata("WAITING_A_PLAY_ORDER")["position"]
	).is_equal( Vector2(2.09999990463256836, 3.09999990463256836) )
	assert_dict(sm.get_state_metadata("MUSIC_IS_PLAYING")).is_empty()
	assert_dict(sm.get_event_metadata("INITIALIZATION_IS_FINISHED")).is_empty()
	assert_dict(sm.get_event_metadata("USER_ASK_TO_PLAY")).is_empty()

	sm = SMStateMachine.new()
	sm.load_json(test_path+"/test_metadata_2.json")
	assert_dict(sm.state_static_metadata).is_not_empty()
	assert_array(sm.state_static_metadata.keys()).is_equal(sm.state_to_int.values())
	assert_dict(sm.event_static_metadata).is_not_empty()
	assert_array(sm.event_static_metadata.keys()).is_equal(sm.evt_to_int.values())

	assert_dict(sm.get_state_metadata("MUSIC_IS_PLAYING")).is_empty()
	assert_dict(sm.get_state_metadata("WAITING_A_PLAY_ORDER")).is_empty()
	assert_dict(sm.get_event_metadata("INITIALIZATION_IS_FINISHED")).is_not_empty()
	assert_str(
		sm.get_event_metadata("INITIALIZATION_IS_FINISHED")["message"]
	).is_equal( "Bonjour" )
	assert_dict(sm.get_event_metadata("USER_ASK_TO_PLAY")).is_empty()

func test_save_dynamic_metadata_into_json():
	var evt_fct = func(_ms: SMStateMachine, _ev:int, mu:int):
		if mu == 0:
			return true
		return false
	
	var api = SMApi.new()
	api.register_evt_fct(Callable(evt_fct))
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/sm_test_2.json")
	
	api.print_debug_flag = false

	sm.register_api(api)

	var json_execution_path = test_user_sm_path + "/test_sm_1_execution.json"

	api.clear()
	sm.start()

	var tmp_id : int = 4
	var event_id : int = 0

	var state_data = {
		"position" : Vector2(2.1, 3.1)
	}

	var event_data = {
		"present" : true,
		"value" : 1.2,
		"count" : 4,
	}

	assert_array(sm.active_states).is_equal([0, 2])
	sm.default_clock.pause()
	sm.state_dynamic_metadata[tmp_id] = state_data 
	sm.event_dynamic_metadata[event_id] = event_data
	sm.save_execution_into_json(json_execution_path)
	sm.reset_dynamic_metadata()
	sm.start()
	assert_bool(sm.state_dynamic_metadata[tmp_id] == state_data).is_false()
	sm.load_execution_from_json(json_execution_path)
	assert_bool(sm.state_dynamic_metadata[tmp_id] == state_data).is_true()
	sm.default_clock.resume()

func test_default_value_in_json():
	var sm_scenario : SMStateMachine = SMStateMachine.new()
	var save_on_missing_uuid = false
	var sm_file = test_path+"/scenario1.json"
	var res : Dictionary = sm_scenario.load_json(sm_file, save_on_missing_uuid)
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	assert_bool(sm_scenario.maximal_number_of_active_states_is_default).is_true()
	assert_bool(sm_scenario.maximal_number_of_events_is_default).is_true()
	assert_bool(sm_scenario.maximal_nb_micro_update_is_default).is_true()

	var test_file : String = test_user_sm_path+"/test_scenario1.json"
	sm_scenario.generate_json_file(test_file)
	assert_file(test_file).exists()
	assert_bool( files_are_identic(test_file, sm_file) ).is_true()

	sm_scenario = SMStateMachine.new()
	save_on_missing_uuid = false
	sm_file = test_path+"/music_player.json"
	res = sm_scenario.load_json(sm_file, save_on_missing_uuid)
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	assert_bool(sm_scenario.maximal_number_of_active_states_is_default).is_false()
	assert_bool(sm_scenario.maximal_number_of_events_is_default).is_false()
	assert_bool(sm_scenario.maximal_nb_micro_update_is_default).is_false()


	sm_scenario = SMStateMachine.new()
	save_on_missing_uuid = false
	sm_file = test_path+"/scenario2.json"
	res = sm_scenario.load_json(sm_file, save_on_missing_uuid)
	assert_bool(res["success"]).is_true()
	assert_bool(res["yet_up_to_date"]).is_false()
	assert_bool(sm_scenario.maximal_number_of_active_states_is_default).is_true()
	assert_bool(sm_scenario.maximal_number_of_events_is_default).is_true()
	assert_bool(sm_scenario.maximal_nb_micro_update_is_default).is_false()
	assert_int(sm_scenario.maximal_nb_micro_update).is_equal(
		sm_scenario.DEFAULT_MAXIMAL_NB_MICRO_UPDATE
	)

	test_file = test_user_sm_path+"/test_scenario2.json"
	sm_scenario.generate_json_file(test_file)
	assert_file(test_file).exists()
	assert_bool( files_are_identic(test_file, sm_file) ).is_true()

func test_empty_state_dictionary():
	var state_machine : SMStateMachine = SMStateMachine.new()
	state_machine.load_json(test_path+"/music_player.json")

	var res = state_machine.get_empty_state_dictionary()
	assert_str(str(res.values())).is_equal("[<null>, <null>, <null>, <null>]")
	assert_int(len(res.keys())).is_equal(4)
	assert_bool("WAITING_END_OF_INITIALIZATION" in res).is_true()
	assert_bool("MUSIC_IS_PLAYING" in res).is_true()
	assert_bool("WAITING_A_PLAY_ORDER" in res).is_true()
	assert_bool("URGENCY_STOP" in res).is_true()

	res = state_machine.get_empty_event_dictionary()
	assert_str(str(res.values())).is_equal("[<null>, <null>, <null>, <null>, <null>]")
	assert_int(len(res.keys())).is_equal(5)
	assert_bool("INITIALIZATION_IS_FINISHED" in res).is_true()
	assert_bool("USER_ASK_TO_PLAY" in res).is_true()
	assert_bool("USER_ASK_TO_STOP" in res).is_true()
	assert_bool("EMERGENCY_OCCURS" in res).is_true()
	assert_bool("USER_ASK_TO_RESTART" in res).is_true()

func test_pre_post_start_update():
	var api = load(test_path + "/test_pre_post_upate_api_impl.gd").new()
	var sm = SMStateMachine.new()
	sm.load_json(test_path+"/test_pre_post_upate.json")
	sm.register_api(api)

	assert_float(api.pre_start_flag).is_equal(0)
	assert_float(api.post_start_flag).is_equal(0)
	assert_float(api.post_update_flag).is_equal(0)
	assert_float(api.pre_update_flag).is_equal(0)

	sm.start()

	assert_float(api.pre_start_flag).is_not_equal(0)
	assert_float(api.post_start_flag).is_not_equal(0)
	assert_float(api.post_update_flag).is_equal(0)
	assert_float(api.pre_update_flag).is_equal(0)
	assert_float(api.pre_start_flag).is_less(api.post_start_flag)

	var time_new : float
	var time_old : float

	time_new = sm.get_time()
	time_old = sm.get_time()
	#assert_float(time_new).is_not_equal(time_old)
	assert_float(sm.get_current_update_time()).is_equal(sm.get_current_update_time())

	sm.update()

	assert_float(api.pre_start_flag).is_not_equal(0)
	assert_float(api.post_start_flag).is_not_equal(0)
	assert_float(api.post_update_flag).is_not_equal(0)
	assert_float(api.pre_update_flag).is_not_equal(0)
	assert_float(api.pre_update_flag).is_less(api.post_update_flag)
	
	time_old = time_new
	time_new = sm.get_time()
	assert_float(time_new).is_not_equal(time_old)
	
	sm.update()
	
	time_old = time_new
	time_new = sm.get_time()
	assert_float(time_new).is_not_equal(time_old)
	assert_float(api.pre_update_flag).is_less(api.post_update_flag)

	sm.update()

	time_old = time_new
	time_new = sm.get_time()
	assert_float(time_new).is_not_equal(time_old)
	assert_float(api.pre_update_flag).is_less(api.post_update_flag)

	sm.update()

	time_old = time_new
	time_new = sm.get_time()
	assert_float(time_new).is_not_equal(time_old)
	assert_float(api.pre_update_flag).is_less(api.post_update_flag)
