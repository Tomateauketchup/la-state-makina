var result : String = ""
var evt_fct : Callable

func _init():
	result = ""

func register_evt_fct(fct : Callable):
	evt_fct = fct

func clear():
	result = ""

#var print_debug = true
var print_debug_flag = false

func add_text(text: String):
	if print_debug_flag:
		print(text)
	result += text + "\n"

###################
# States 
###################

# out functions
func fct_out(_sm: SMStateMachine, _state:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	add_text("out " + str(_state) + " - " + str(_trans_ids) + " - " + str(_nb_micro_update))
	return stop_update

# deactivation functions
func fct_deactivation(_sm: SMStateMachine, _state:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	add_text("deactivation " + str(_state) + " - " + str(_trans_ids) + " - " + str(_nb_micro_update))
	return stop_update

# In functions
func fct_in(_sm: SMStateMachine, _state:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	add_text("in " + str(_state) + " - " + str(_trans_ids) + " - " + str(_nb_micro_update))
	return stop_update

# activation functions
func fct_activation(_sm: SMStateMachine, _state:int, _trans_ids : Array[int], _nb_micro_update:int) -> bool:
	var stop_update = false
	add_text("activation " + str(_state) + " - " + str(_trans_ids) + " - " + str(_nb_micro_update))
	return stop_update

# Micro Update functions
func fct_micro_update(_sm: SMStateMachine, _state:int, _nb_micro_update:int) -> bool:
	var stop_micro_update = false
	add_text("micro_update " + str(_state) + " - " + str(_nb_micro_update))
	return stop_micro_update
	
# update 
func fct_update(_sm: SMStateMachine, _state_id:int, _delay:float) -> bool:
	var stop_update = false
	assert(_delay>0)
	assert(_delay<1.0) # Update is in seconds, so it should be lesser than 1 second
	add_text("update " + str(_state_id))
	return stop_update


###################
# Events
###################
func fct_evt(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	add_text("test evt " + str(_event) + " - " + str(_nb_micro_update))
	return evt_fct.call(_sm, _event, _nb_micro_update)

###################
# transitions
###################
func fct_trans(_sm: SMStateMachine, _trans:int, _nb_micro_update:int) -> bool:
	var stop_update = false
	add_text("trans " + str(_trans) + " - " + str(_nb_micro_update))
	return stop_update
