## This file is the API of the state machine defined in the following Json File :[br]
## res://addons/la-state-makina/src/state_machine/test/test_pre_post_upate.json[br][br]
##
## This file was automatically generated by the script
## res://addons/la-state-makina/src/state_machine/sm_state_machine_data.gd[br][br]
##
## Read the following tutorial for more detail how to use Machine State API : [br]
## @tutorial: res://addons/la-state-makina/README.md
## @tutorial: https://gitlab.com/Tomateauketchup/la-state-makina

#
# Don't modify that script. Extend it instead.
#

# If you need to change the parent class of that generated API
# add or modify in the JSON description of you state machine the following
# field :
# "extends_class": "THE_NEW_PARENT_CLASS"
extends RefCounted


#
# API for Micro Update functions of some states
#


#
# API for Updates functions of some states
#


#
# API for event functions used to test if an event have to be raised.
#


#
# API for transition functions
#

