class_name UnitTestSMClock
extends GdUnitTestSuite


func before():
	pass

func after():
	pass

func wait(n):
	var a = 0
	for i in range(n):
		a += i

func test_clock():
	var clock = SMClock.new()
	clock.start()
	var old_time 
	var time = clock.get_time()
	for i in range(20):
		old_time = time
		wait(10)
		time = clock.get_time()
		assert_bool(time > old_time).is_true()
	clock.pause()
	time = clock.get_time()
	for i in range(20):
		old_time = time
		wait(10)
		time = clock.get_time()
		assert_bool(time == old_time).is_true()
	time = clock.get_time()
	clock.resume()
	for i in range(20):
		old_time = time
		wait(10)
		time = clock.get_time()
		assert_bool(time > old_time).is_true()

func test_duplicate():
	var clock_1 = SMClock.new()
	clock_1.start()
	var clock_2 = clock_1.duplicate()
	var time_1
	var time_2
	for i in range(100):
		time_1 = clock_1.get_time()
		time_2 = clock_2.get_time()
		assert_bool( time_1 <= time_2 ).is_true()
		time_1 = clock_2.get_time()
		time_2 = clock_1.get_time()
		assert_bool( time_1 <= time_2 ).is_true()

func test_de_serialize():
	var clock_1 = SMClock.new()
	clock_1.start()
	clock_1.pause()
	var ser_1 = clock_1.serialize()
	var clock_2 = SMClock.new()
	clock_2.start()
	clock_2.deserialize(ser_1)
	var ser_2 =  clock_2.serialize()
	assert_dict(ser_1).is_equal(ser_2)


