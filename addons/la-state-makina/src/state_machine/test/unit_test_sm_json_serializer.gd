class_name UnitTestSMJsonSerializer
extends GdUnitTestSuite


func before():
	pass

func after():
	pass

func test_de_serialize():
	var value = {
		1 : [3, 4, "cool"],
		"enfin" : {
			"2D" : Vector2(1.1,2), "3D" : Vector3(1,2.2,3), "4D" : Vector4(1.1,2.2,3.3,4.4)
		},
		3.4 : true,
		StringName("toto") : false,
		"gg" : StringName("Essai"),
		"other" : null
	}
	var ser : String = SMJsonSerializer.serialize(value)
	var errors : Array[String] = []
	var deser_value = SMJsonSerializer.deserialize(ser, errors)
	assert_array(errors).is_empty()
	assert_bool(deser_value == value).is_true()

	value = {
		"essai" : {"toto": 1, "titi":Vector2(3.5,9)},
		"tutu" : "aaa",
		"bbb" : 3.4
	}
	ser = SMJsonSerializer.serialize(value)
	errors = []
	deser_value = SMJsonSerializer.deserialize(ser, errors)
	assert_array(errors).is_empty()
	assert_bool(deser_value == value).is_true()

	value = {
		"essai" : {"toto": 1, "titi":Vector2(3.5,9)},
		"tutu" : "aaa",
		"bbb" : 3.4,
		"_type" : 4
	}
	ser = SMJsonSerializer.serialize(value)
	errors = []
	deser_value = SMJsonSerializer.deserialize(ser, errors)
	assert_array(errors).is_empty()
	assert_bool(deser_value == value).is_true()

	value = {
		"essai" : {"toto": 1, "titi":Vector2(3.5,9)},
		"tutu" : "aaa",
		"bbb" : 3.4,
		"_value" : 4
	}
	ser = SMJsonSerializer.serialize(value)
	errors = []
	deser_value = SMJsonSerializer.deserialize(ser, errors)
	assert_array(errors).is_empty()
	assert_bool(deser_value == value).is_true()
	
	value = {
		"essai" : {"toto": 1, "titi":Vector2(3.5,9)},
		"tutu" : "aaa",
		"bbb" : 3.4,
		"_value" : 4,
		"_type" : 9
	}
	ser = SMJsonSerializer.serialize(value)
	errors = []
	deser_value = SMJsonSerializer.deserialize(ser, errors)
	assert_array(errors).is_empty()
	assert_bool(deser_value == value).is_true()
