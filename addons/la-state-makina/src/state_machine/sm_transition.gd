## This class implements a transition of a state machine.
##
## A state machine is a special graph with vertices and "generalized" arcs. A
## transition is the name given to a "generalized" arc.
## [br][br]
##
## More precisely, a transition is a triplet (origins, event, ends) where
## origins is a set of states (see [SMState]), ends is a set of states, and
## event is an event (see [SMEvent]).
## [br][br]
##
## In the state machine, states can be active or not.
## On a [method SMStateMachine.micro_update], the machine will cross all
## transitions that can be crossed.
## If a transition is crossed, all its origins become inactive, and all its
## ends become active.[br]
## If a state has to be changed to active and inactive at the same time
## (because it is an origin and an end of two different transitions), then the
## state is changed to active.
## [br][br]
##
## A transition can be crossed if the following conditions occur at the same
## time:[br]
## 1) Its event has been raised (see [SMEvent])[br]
## 2) The [member SMTransition.type] of the transition is
##    [constant SMTransition.OR] and at least one of its origins is active,[br]
##    or
##    [br]
##    The [member SMTransition.type] of the transition is
##    [constant SMTransition.AND] and all its origins are active.
## [br][br]
##
## See [SMStateMachine] for more details.
class_name SMTransition
extends RefCounted

# The documentation was created with the assistance of ChatGPT 3.5
# (chat.openai.com - 12/2023).
# The used prompts were : 
#  - "Traduit en anglais les phrases suivantes."
#  - "You are an expert in informatics in state machina and automata. Improve
#    the english of the following sentences. "

## The identifier of the transition.
## [br][br]
##
## This is the integer used internally by the state machine to identify this
## transition. This index is also the transition index of the array
## [member SMStateMachine.transitions] of [SMStateMachine] where this
## transition is stored.[br]
## [br][br]
##
## Ids and uuids are in bijection.
var id : int

## The Universal Unique Identifier associated with that transition.
## [br][br]
##
## This identifier is used by the scene associated with the State Machine.
## It allows the user to change, in the JSON, the data of that transition
## without having to modify the scene.
## [br][br]
##
## Ids and uuids are in bijection.
var uuid : String

## The type of a transition.
##
## See [member SMTransition.type] for more details.
enum TransitionType {
	OR, ## To cross the transition, just one of the origins has to be active .
	AND ## To cross the transition , all the origins have to be active.
}

## Defines the type of this transition.
## [br][br]
##
## If the type is equal to [constant SMTransition.OR], then one condition
## to cross this transition is having at least one of its origins active.
## [br][br]
##
## If the type is equal to [constant SMTransition.AND], then one condition
## to cross this transition is having all of its origins active.
## [br][br]
##
## The other condition is having the [member SMTransition.event] raised.
## See [SMTransition] for more details.
var type : TransitionType = TransitionType.AND

## This array contains the state ids that constitute the origins of the 
## transition. The list is sorted. [br]
##
## Refer to [member SMState.id] for additional details.
var origin_ids : Array[int]   # This array is sorted
## The array of state names that make up the origins of the transition.
## This array is sorted using [member SMTransition.origin_ids].[br]
##
## For more details about a state name, refer to [member SMState.name].
var origins: Array[StringName] # Soreted as origin_ids
## The array of state UUIDs that form the origins of the transition. [br]
## This array is sorted using [member SMTransition.origin_ids].
##
## For more details about a state UUID, refer to [member SMState.uuid].
var origin_uuids: Array[StringName] = [] # Soreted as origin_ids
## Determine whether the transition origins include all states of the machine.
## [br][br]
##
## In a JSON description, origins can be specified as "*". In this case, the 
## origins encompass all states of the machine. To implement this scenario, 
## set the boolean to true. Additionally, include in
## [member SMTransition.origin_ids], [member SMTransition.origins], and 
## [member SMTransition.origin_uuids] all the states of the machine.
var origins_are_all_states : bool

## The array of state IDs that make up the ends of the transition.
## This array is sorted. [br]
##
## For more details, refer to [member SMState.id].
var end_ids : Array[int]      # This array is sorted
## The array of state names that form the ends of the transition.
## This array is sorted using [member SMTransition.end_ids]. [br]
##
## For more details about a state name, refer to [member SMState.name].
var ends:  Array[StringName]  # Soreted as origin_ids
## The array of state UUIDs that make up the ends of the transition. [br]
## This array is sorted using [member SMTransition.end_ids].
##
## For more details about a state UUID, refer to [member SMState.uuid].
var end_uuids: Array[StringName] = [] # Soreted as origin_ids
## The event name associated with this transition.
##
## One condition to cross the transition is having that event raised.
##
## (Refer to [member SMTransition.type] for additional conditions.)
var event: StringName
## The event id of the transition.
##
## See [member SMTransition.event] for more details.
var event_id : int

## The functions to execute when that transition is crossed.
var fcts : Array[Callable] = []
## The name of the functions defined in the JSON description.
## See [member SMTransition.fcts] for more details.
var fcts_names : Array[String] = []

## Returns the string representation of the transition type.
func type_str() -> String:
	if type == TransitionType.OR:
		return "or"
	return "and"

## Converts the transition into a string for printing.
func _to_string():
	var origins_str = ""
	for origin in origins:
		origins_str += origin + ", "
	var ends_str = ""
	for end in ends:
		ends_str += end + ", "
	return "(" + origins_str + "-> " + ends_str + " : " + String(event) + " - t:" + type_str() + ")"

# Don't use that function. Use [method SMMachineState.add_origin_to_transition]
# of [SMStateMachine] instead.
# This is crucial as the adjacency list of SMStateTransition also needs to be
# updated.
func _add_origin(state):
	var origin_id : int  = state.id
	var origin_name : StringName = state.name
	var origin_uuid : String = state.uuid
	if origin_id in origin_ids:
		return false
	var pos : int = origin_ids.bsearch(origin_id)
	origin_ids.insert(pos, origin_id)
	origins.insert(pos, origin_name)
	origin_uuids.insert(pos, uuid)
	return true

# Don't use that function. Use [method SMMachineState.add_end_to_transition]
# of [SMStateMachie] instead.
# This is crucial as the adjacency list of SMStateTransition also needs to be
# updated.
func _add_end(state):
	var end_id : int  = state.id
	var end_name : StringName = state.name
	var end_uuid : String = state.uuid
	if end_id in end_ids:
		return false
	var pos : int = end_ids.bsearch(end_id)
	end_ids.insert(pos, end_id)
	ends.insert(pos, end_name)
	end_uuids.insert(pos, uuid)
	return true

## Serialize the data of that transition in a dictionary that can be stored in
## Json format.
func serialize():
	var result = {}
	result["uuid"] = uuid
	if origins_are_all_states:
		result["origins"] = "*"
	else:
		result["origins"] = SMState._collect_fcts(origins)
	result["ends"] = SMState._collect_fcts(ends)
	result["event"] = event
	result["type"] = type_str()
	if(len(fcts_names)>0):
		result["fcts"] = SMState._collect_fcts(fcts_names)
	return result

## Create a copy of the transition and return it.
## [br][br]
##
## [param keep_execution_state]: If set to true, the execution states are
## retained. If set to false, the execution states are not initialized. [br]
## [param keep_api]: If set to true, the API references are retained; otherwise,
## the API reference is not initialized, and a state machine API should be
## registered with the [method SMStateMachine.register_api] method of 
## [SMStateMachine].
func clone(
	keep_execution_state : bool = true, keep_api : bool = false
	) -> SMTransition:
	var  res : SMTransition = SMTransition.new()
	res.id = int(id)
	res.uuid = String(uuid)
	res.type = type

	res.origin_ids = origin_ids.duplicate(true)
	res.origins = origins.duplicate(true)

	res.origin_uuids = origin_uuids.duplicate(true)
	res.origins_are_all_states = bool(origins_are_all_states)

	res.end_ids = end_ids.duplicate(true)

	res.ends = ends.duplicate(true)
	res.end_uuids = end_uuids.duplicate(true)

	res.event = StringName(event)
	res.event_id = int(event_id)

	res.fcts = []
	if keep_api:
		res.fcts = res.fcts.duplicate(true)
	res.fcts_names = fcts_names.duplicate(true)

	return res

