## This class represents a state within a state machine.
##
## A state machine is a specialized graph with vertices and generalized arcs.
## In this context, a state refers to the name assigned to a vertex in the
## special graph.[br]
## The state is identified by a name (a string) in the JSON description, by an
## ID (an integer) in the state machine algorithm, and by a Universal Unique 
## Identifier (UUID - a string) in its graphical representation in Godot.[br]
## Each state is unique due to its identifiers.
## [br][br]
##
## See [SMStateMachine] for more details.[br][br]
##
class_name SMState
extends RefCounted

# The documentation was created with the assistance of ChatGPT 3.5
# (chat.openai.com - 12/2023).
# The used prompts were : 
#  - "Traduit en anglais les phrases suivantes."
#  - "You are an expert in informatics in state machina and automata. Improve
#    the english of the following sentences. "

#
# Static Data
#

## The state's name.
##
## This name is utilized by the user to identify states in the JSON description
## of the state machine.[br][br]
##
## Names, Ids, and uuids are in bijection.
var name : StringName

## The identifier of the state.
##
## This integer serves as the internal identifier used by the state machine to
## uniquely identify this state. It also corresponds to the state index within
## the [member SMStateMachine.states] array of the [SMStateMachine] where this
## state is stored.
## [br][br]
##
## Names, ids, and uuids are in bijection.
var id : int

## The Universal Unique IDentifier associated with this state.
##
## This identifier is utilized by the scene associated with the State Machine.
## It enables the user to change the state's name in the JSON without requiring
## modifications to the scene.[br][br]
##
## Names, IDs, and UUIDs are in bijection.
var uuid : String

## An array of "in" functions.
##
## These functions are executed each time a transition is taken during a 
## micro-update, with the terminal state being this state ([self]). [br]
## For more details, refer to [SMStateMachine].
var fcts_in : Array[Callable] = []
## The names of "in" functions as defined in the JSON description.
## Refer to fcts_in for more details.
var fcts_in_names : Array[String] = []

## An array of "activation" functions.
##
## During a micro-update, each time a state becomes active, these functions are
## executed. If a transition is crossed and enters an already active state,
## these functions are not executed. In such cases, only "in" functions are run.
var fcts_activation : Array[Callable] = []
## The names of "activation" functions as defined in the JSON.
## Refer to [member State.fcts_activation] for more details.
var fcts_activation_names : Array[String] = []

## An array of "out" functions.
##
## These functions are executed each time a transition is taken during a
## micro-update, when this state ([self]) is the origin of the transition.
## For more details, refer to [SMStateMachine].
var fcts_out : Array[Callable] = []
## The names of "out" functions as defined in the JSON description.
var fcts_out_names : Array[String] = []

## An array of "deactivation" functions.
##
## During a micro-update, each time a state becomes inactive, these functions
## are executed.
## 
## These functions run only when a state changes its state from active to
## inactive. If two transitions are crossed simultaneously -- one having this
## state ([self]) as the origin and another having this state as the end --
## then, the deactivation functions are not executed.
var fcts_deactivation : Array[Callable] = []
## The names of "deactivation" functions as defined in the JSON description.
var fcts_deactivation_names : Array[String] = []

## An array of "micro-update" functions.
##
## At the beginning of each micro-update, if this state ([self]) is active, all
## these functions are executed.
var fcts_micro_update : Array[Callable] = []
## The names of "micro-update" functions as defined in the JSON description.
var fcts_micro_update_names : Array[String] = []

## An array of "update" functions.
##
## Once all the micro-updates have converged to a set of active states, the
## "update" functions associated with each of these active states are executed.
## These functions run at the end of the convergence but never during it.
## Therefore, if a state has been activated and deactivated during the
## micro-updates but does not end up active, its update function will not be
## executed.
var fcts_update : Array[Callable] = []
## The names of "update" functions as defined in the JSON.
## See [member State.fcts_activation] for more details.
var fcts_update_names : Array[String] = []

#
# Data For Machine State Exection
#

## This flag indicates that the state is active.
var is_active : bool = false
## This represents the most recent activation time of the state.
## [br][br]
##
## By default, it corresponds to the elapsed time in microseconds since the
## [method SMClock.start] of the internal clock of the state machine.
## You can define your own time function by modifying the
## [member SMStateMachine.time_fct] in the [SMStateMachine] class.
var last_activation_time : int = 0
## This represents the most recent deactivation time of the state.
## [br][br]
##
## Refer to [member SMState.last_activation_time] for more details.
var last_deactivation_time : int = 0


## This represents the most recent activation time in terms of update and
## micro-update counters.
## [br][br]
##
## It consists of a pair of integers. The first value is the number of updates
## since the initiation of the state machine with the
## [method SMStateMachine.start] function.
## The second value is the number of micro-updates since the beginning of the
## last [method SMStateMachine.update].
var last_activation_time_cpts : Array[int] = [-1, -1] # update cpt , micro-update cpt
## This is the last deactivation time in update and micro-update counters.
##
## This is a pair on integer. The first value is the number of updates since the
## starting of the state machine. The second value is the number of micro-update
## since the begining of the last [method SMState.update].

## This represents the most recent deactivation time in terms of update and
## micro-update counters.
## [br][br]
##
## It consists of a pair of integers. The first value is the number of updates
## since the initiation of the state machine with the 
## [method SMStateMachine.start] function. The second value is the number of
## micro-updates since the beginning of the last [method SMStateMachine.update].
var last_deactivation_time_cpts : Array[int] = [-1, -1] # update cpt , micro-update cpt

## This flag indicates that the state was active at the end of the previous
## update.
var was_active_update : bool = false
## This flag indicates that the state was active at the end of the previous
## micro-update.
var was_active_micro_update : bool = false

#
# Methods
#

## Returns true if this state has already been activated.
func has_been_activated_once():
	return last_activation_time > 0

## Returns true if this state has already been deactivated.
func has_been_deactivated_once():
	return last_deactivation_time > 0

## Returns true if this state is currently active and was not active at the
## end of the previous update.
## [br][br]
##
## Caution: This function can be risky if used during the micro-update process.
## The list of active states is not yet stable during this phase.
## [br][br]
##
## It is advisable to use this function after the micro-update process. For
## instance, you can incorporate it into the update functions of 
## [member SMState.fcts_update], or within the _post_update() function of your
## state machine API, or after the execution of 
## [method SMStateMachine.update].
## [br][br]
##
## If you need to use it during the micro-update process, consider using the
## method [method SMState.rising_edge_on_micro_update] instead.
func rising_edge_on_update() -> bool :
	return not was_active_update and is_active

## Returns true if this state is currently not active and was active at the
## end of the previous update.
## [br][br]
##
## Caution: This function can be risky if used during the micro-update process.
## The list of active states is not yet stable during this phase.
## [br][br]
##
## It is advisable to use this function after the micro-update process. For
## instance, you can incorporate it into the update functions of 
## [member SMState.fcts_update], or within the _post_update() function of your
## state machine API, or after the execution of 
## [method SMStateMachine.update].
## [br][br]
##
## If you need to use it during the micro-update process, consider using the
## method [method SMState.falling_edge_on_micro_update] instead.
func falling_edge_on_update() -> bool :
	return was_active_update and not is_active

## Returns true if this state is currently active and was not active at the
## end of the previous micro-update.
func rising_edge_on_micro_update() -> bool :
	return not was_active_micro_update and is_active

## Returns true if this state is currently not active and was active at the
## end of the previous micro-update.
func falling_edge_on_micro_update() -> bool :
	return was_active_micro_update and not is_active

## Returns the sting value of the name of this state.
func _to_string():
	return String(name)

# A utility function to simplify the serialization process.
static func _collect_fcts(array):
	var result = ""
	if len(array) == 1:
		result = array[0]
	elif len(array)>1:
		result = array.duplicate()
	return result

## Serializes the state description into a Dictionary that can be converted into
## a JSON file.
func serialize():
	var result = {}
	result["name"] = name
	result["uuid"] = uuid
	if len(fcts_in_names)>0:
		result["fcts_in"] = _collect_fcts(fcts_in_names)
	if len(fcts_out_names)>0:
		result["fcts_out"] = _collect_fcts(fcts_out_names)
	if len(fcts_activation_names)>0:
		result["fcts_activation"] = _collect_fcts(fcts_activation_names)
	if len(fcts_deactivation_names)>0:
		result["fcts_deactivation"] = _collect_fcts(fcts_deactivation_names)
	if len(fcts_update_names)>0:
		result["fcts_update"] = _collect_fcts(fcts_update_names)
	if len(fcts_micro_update_names)>0:
		result["fcts_micro_update"] = _collect_fcts(fcts_micro_update_names)
	return result

## Serializes the execution state of the state into a Dictionary that can
## be converted into a JSON file.
func serialize_execution_state() -> Dictionary:
	var result : Dictionary = {
		"is_active" : is_active,
		"last_actuvation_time" : last_activation_time,
		"last_deactivation_time" : last_deactivation_time,
		"last_activation_time_cpts" : last_activation_time_cpts,
		"last_deactivation_time_cpts" : last_deactivation_time_cpts,
		"was_active_update" : was_active_update,
		"was_active_micro_update" : was_active_micro_update,
	}
	return result

## Tests if the state has the same execution state of a given state.
func has_same_execution_state(state : SMState) -> bool:
	return (
		(is_active == state.is_active) and
		(last_activation_time == state.last_activation_time) and
		(last_deactivation_time == state.last_deactivation_time) and
		(last_activation_time_cpts == state.last_activation_time_cpts) and
		(last_deactivation_time_cpts == state.last_deactivation_time_cpts ) and
		(was_active_micro_update == state.was_active_micro_update) and
		(was_active_update == state.was_active_update)
	)

## Initializes the execution states with the serialized data stored in the
## [param ser] dictionary. If that dictionary cannot be converted into execution
## states due to errors, the errors are stored in the array [param errors].
func deserialize_execution_state(ser:Dictionary, errors:Array[String]):
	is_active = SMTools.deserialize_key(ser, "is_active", "bool", errors)
	last_activation_time = SMTools.deserialize_key(
		ser, "last_actuvation_time", "int", errors
	)
	last_deactivation_time = SMTools.deserialize_key(
		ser, "last_deactivation_time", "int", errors
	)
	last_activation_time_cpts = SMTools.array_to_arrayint( SMTools.deserialize_key(
		ser, "last_activation_time_cpts", "pair of int", errors
	) )
	last_deactivation_time_cpts = SMTools.array_to_arrayint( SMTools.deserialize_key(
		ser, "last_deactivation_time_cpts", "pair of int", errors
	) )
	was_active_update = SMTools.deserialize_key(
		ser, "was_active_update", "bool", errors
	)
	was_active_micro_update = SMTools.deserialize_key(
		ser, "was_active_micro_update", "bool", errors
	)

## Creates a copy of this state and returns it.
## [br][br]
##
## [param keep_execution_state]: If set to true, the execution states are
## retained. If set to false, the execution states are not initialized.[br]
## [param keep_api]: If set to true, the API references are retained. Otherwise,
## the API reference is not initialized, and a state machine API should be
## registered with the [method SMStateMachine.register_api] method of
## [SMStateMachine].
func clone(
	keep_execution_state : bool = true, keep_api : bool = false
) -> SMState:
	var res : SMState = SMState.new()
	res.name = StringName(name)
	res.id = int(id)
	res.uuid = String(uuid)

	res.fcts_in = []
	res.fcts_out = []
	res.fcts_activation = []
	res.fcts_deactivation = []
	res.fcts_micro_update = []
	res.fcts_update = []
	if keep_api:
		res.fcts_in = fcts_in.duplicate(true)
		res.fcts_out = fcts_out.duplicate(true)
		res.fcts_activation = fcts_activation.duplicate(true)
		res.fcts_deactivation = fcts_deactivation.duplicate(true)
		res.fcts_micro_update = fcts_micro_update.duplicate(true)
		res.fcts_update = fcts_update.duplicate(true)

	res.fcts_in_names = fcts_in_names.duplicate(true)
	res.fcts_out_names = fcts_out_names.duplicate(true)
	res.fcts_activation_names = fcts_activation_names .duplicate(true)
	res.fcts_deactivation_names = fcts_deactivation_names.duplicate(true)
	res.fcts_micro_update_names = fcts_micro_update_names.duplicate(true)
	res.fcts_update_names = fcts_update_names.duplicate(true)

	if keep_execution_state:
		res.is_active = bool(is_active)
		res.last_activation_time = int(last_activation_time)
		res.last_deactivation_time = int(last_deactivation_time)
		res.last_activation_time_cpts = last_activation_time_cpts.duplicate(true)
		res.last_deactivation_time_cpts = last_deactivation_time_cpts.duplicate(true)

		res.was_active_update = bool(was_active_update)
		res.was_active_micro_update = bool(was_active_micro_update)

	return res
