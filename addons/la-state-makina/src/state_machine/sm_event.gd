## This class implements the event of a state machine.
##
## An event is defined by a name (a string) and a list of functions.
## [br][br]
##
## The event is considered raised if at least one function in that list returns
## true. Otherwise, we say the event is not raised.
## [br][br]
##
## Events are used in [SMTransition] to determine if a transition can be
## crossed. One condition to cross a transition is having its event raised.
## See [SMTransition] for more details.[br][br]
class_name SMEvent
extends RefCounted

# The documentation was created with the assistance of ChatGPT 3.5
# (chat.openai.com - 12/2023).
# The used prompts were : 
#  - "Traduit en anglais les phrases suivantes."
#  - "You are an expert in informatics in state machina and automata. Improve
#    the english of the following sentences. "

## The name of the event.
## [br][br]
##
## This name is used by the user to identify the event in the JSON description
## of the state machine.
## [br][br]
##
## Names and ids are in bijection.
var name : StringName
## The identifier of the event.
##
## This is the integer used internally by the state machine to identify this
## event. This index is also the event index of the array
## [member SMStateMachine.events] of [SMStateMachine] where this event is
## stored.
## [br][br]
##
## Names and ids are in bijection.
var id: int

## The functions to execute to determine if this event has to be raised.
## [br][br]
##
## If at least one of these functions returns true, the event has to be raised.
## [br][br]
##
## See [SMEvent] for more details.
var fcts : Array [Callable] = []
## The names of the functions defined in the JSON description.
## See [member SMTransition.fcts] for more details.
var fcts_names : Array[String] = []

## Returns the sting value of the name of this event.
func _to_string() -> String :
	return String(name)

## Serializes the event description into a Dictionary that can be converted into
## a JSON file.
func serialize() -> Dictionary:
	var result = {}
	result["name"] = name
	result["fcts"] = SMState._collect_fcts(fcts_names)
	return result

## Creates a copy of this event and returns it.
## [br][br]
##
## [param keep_execution_state]: If set to true, the execution states are
## retained. If set to false, the execution states are not initialized.[br]
## [param keep_api]: If set to true, the API references are retained. Otherwise,
## the API reference is not initialized, and a state machine API should be
## registered with the [method SMStateMachine.register_api] method of 
## [SMStateMachine].
func clone(
	_keep_execution_state : bool = true, keep_api : bool = false
) -> SMEvent:
	var res : SMEvent = SMEvent.new()
	res.name = StringName(name)
	res.id = id
	if keep_api:
		res.fcts = fcts.duplicate(true)
	else:
		res.fcts = []
	res.fcts_names = fcts_names.duplicate(true)
	return res
