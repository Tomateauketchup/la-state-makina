## This class implements a clock for the state machine.
##
## This class is used to timestamp the execution of the state machine.[br]
## The time used by this class is obtained through [method Time.get_ticks_usec].
## [br][br]
##
## This class allows you to pause the clock when needed. This functionality is
## useful when you need to pause your application.[br][br]
## 
## Here's an example of usage:
## [codeblock]
## clock : SMClock = SMClock.new()
## clock.start()
##
## current_time_us : int = clock.get_time()
##
## clock.pause()
## clock.resume()
##
## current_time_us = clock.get_time()
## [/codeblock]
class_name SMClock
extends RefCounted

# The documentation was created with the assistance of ChatGPT 3.5
# (chat.openai.com - 12/2023).
# The used prompts were : 
#  - "Traduit en anglais les phrases suivantes."
#  - "You are an expert in informatics in state machina and automata. Improve
#    the english of the following sentences. "

# This boolean is set to False when the clock is paused or not started.
var _is_on : bool
# The system time (using [Time.get_ticks_usec]) in microseconds of the last
# resume time.
var _last_resume_time : int = 0
# The elapsed time since the last [method SMClock.start], excluding any periods
# during which the clock was paused.
var _elapsed_time : int = 0

## Constructs a new clock. This new clock needs to be started with the
## [method SMClock.start] method.
func _init():
	_is_on = false
	_last_resume_time = 0
	_elapsed_time = 0

## Starts the clock.[br][br]
##
## The starting time is set to 0.[br][br]
##
## Do not use this function if you want to resume a paused clock.
func start():
	_elapsed_time = 0
	resume()

## Restarts the clock.[br][br]
## 
## Same as [method SMClock.start].[br][br]
##
## Do not use this function if you want to resume a paused clock.
func restart():
	start()

## Resumes a paused clock.
func resume():
	_last_resume_time = Time.get_ticks_usec()
	_is_on = true

## Returns the time in microseconds since [method SMClock.start] was called on
## the clock, excluding any periods during which the clock was paused using the
## [method SMClock.pause] method.
func get_time() -> int :
	var res = _elapsed_time
	if _is_on:
		res += (Time.get_ticks_usec() - _last_resume_time)
	return res

## Suspends the clock time. Elapsed time is frozen until the clock is resumed 
## with the [method SMClock.resume()] method.
func pause():
	_elapsed_time = get_time() 
	_is_on = false

## Returns a copy of that clock.[br][br]
##
## The parameter [param _deep] is ignored.
func duplicate(_deep:bool = false) -> SMClock:
	var res : SMClock = SMClock.new()
	res._is_on = _is_on
	res._last_resume_time = _last_resume_time
	res._elapsed_time = _elapsed_time
	return res

## Pauses the clock and then initializes the time using the dictionary passed in
## as a parameter. The time remains paused, and you need to resume it manually
## by using the [method SMClock.resume] function.
func deserialize(json_dict:Dictionary):
	pause()
	_elapsed_time = json_dict["elapsed_time"]
	_last_resume_time = 0

## Serializes the clock execution into a Dictionary that can be converted into
## a JSON file.
func serialize() -> Dictionary:
	var res : Dictionary = {}
	res["elapsed_time"] = get_time()
	return res
