## Small internal tools for the State Machine
## 
## You can pass. This probably doesn't fit your needs.
class_name SMTools

## Desrialize the value associated to a given key of a given dictionary.
## The type of the serialized data is given by the parameter [param type].[br][br]
##
## [param ser] : The input dictionary.[br]
## [param key] : The key of the value to deserialize.[br]
## [param type] : the type of the value to deserialize.[br]
## [param errors] : An array to store obtained errors during the process.
static func deserialize_key(
	ser:Dictionary, key:String, type:String, errors:Array[String]
) -> Variant:
	var res_errors : Array[String] = [] 
	assert(type in ["bool", "float", "int", "String", "Array", "pair of int"])

	if not key in ser:
		var text : String
		text = "Fails to deserialize '%s'. '%s' is missing."%key
		res_errors.append(text)
	var res = null
	if len(res_errors)==0:
		res = ser[key]
		if not(
			( (type == "bool") and (typeof(res) == TYPE_BOOL) ) or
			( (type == "float") and (typeof(res) == TYPE_FLOAT) ) or
			( (type == "int") and (typeof(res) == TYPE_INT) or (typeof(res) == TYPE_FLOAT) ) or
			( (type == "String") and (typeof(res) == TYPE_STRING) ) or
			( (type == "Array") and (typeof(res) == TYPE_ARRAY) ) or
			( 
				(type == "pair of int") and 
				(typeof(res) == TYPE_ARRAY) and (len(res)==2) and 
				((typeof(res[0]) == TYPE_FLOAT) or (typeof(res[0]) == TYPE_INT)) and 
				((typeof(res[1]) == TYPE_FLOAT) or (typeof(res[1]) == TYPE_FLOAT))
			)
		):
			var text : String
			text = (
				"Fails to deserialize the value %s of the key %s."%[res, key] + 
				"Expected type is : %s."%type
			)
			res_errors.append(text)
		elif ( (type == "int") and (typeof(res) == TYPE_FLOAT) or (typeof(res) == TYPE_FLOAT) ):
			res = int(res)
		elif ( 
				(type == "pair of int") and 
				(typeof(res) == TYPE_ARRAY) and (len(res)==2) and 
				((typeof(res[0]) == TYPE_FLOAT) or (typeof(res[0]) == TYPE_INT)) and 
				((typeof(res[1]) == TYPE_FLOAT) or (typeof(res[1]) == TYPE_FLOAT))
			):
			res = [int(res[0]), int(res[1])]
	if len(res_errors) > 0:
		for err in res_errors:
			errors.append(err)
		if type == "bool":
			return false
		if type == "float":
			return 0.0
		if type == "Array":
			return []
		if type == 'int':
			return 0
		if type == "pair of int":
			return [0, 0]
		return ""
	return res

## Convert an array of numeric values into an array on integers.
static func array_to_arrayint(v : Array) -> Array[int]:
	var res : Array[int] = []
	for u in v:
		res.append(int(u))
	return res
