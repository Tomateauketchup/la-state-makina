## This class implement rising edges in the state machine.
##
## This class takes as parameter an event function (see below the definition 
## of event function) and returns true if the value made a raising edge during
## the last micro-update.
##
## This class is dedicated to help developpers to implement a state machine API
## and to symplify machine state graph.
##
## Indeed, a state machine :
##
## [codeblock] 
## -----------  val == 0  -----------  val = 1  ----------
## | state A |----------->| State B |---------->| State C|
## -----------            -----------           ----------
## [/codeblock] 
##
## can be replaced by 
##
## 
## [codeblock] 
## -----------  val is rising  -----------
## | state A |---------------->| State B |
## -----------                 -----------
## [/codeblock] 
##
## For example, if you need to implement the event function 'on_rising_val', 
## testing if a val is rising, then you can implement the API by using this 
## code :
## [/codeblock]
## var on_raising_a : SMFallingRisingEdge
##
## var _init():
##    var playing_is_active = func (_sm: SMStateMachine, _event:int, _nb_micro_update:int):
##        # CODE TO OBTAIN THE VALUE OF val
##        return val
##    on_playing = SMFallingRisingEdge.new(playing_is_active)
##
## func _post_start():
##    on_playing.start()
##
## func on_playing(_sm: SMStateMachine, _event:int, _nb_micro_update:int):
##    return on_playing.is_rising(_sm, _event, _nb_micro_update)
##
## var rising_edge_on_playing_is_active = RisingEdge.new(playing_is_active)
## [/codeblock]
class_name SMFallingRisingEdge
extends RefCounted

var _value_fct : Callable

var before_update_counter:int
var before_micro_update_counter:int
var before_value : bool

var update_counter:int
var micro_update_counter:int
var value : bool

var init_value : bool

## Construct a object that convert value to rising edges.
## [br][br]
##
## [param callable_function]: the function use to obtain the value to convert 
## in rising edge.[br]
## [param _init_value]: the default value used to initalize the data.
func _init(callable_function:Callable, _init_value:bool = false):
	_value_fct = callable_function
	init_value = _init_value
	restart()

## Reinitialize the internal data.
func restart():
	before_update_counter = -1
	before_micro_update_counter = -1
	before_value = init_value
	
	update_counter = -1
	micro_update_counter = -1
	value = init_value

# Update the data.
func _update(_sm: SMStateMachine, _event:int, _nb_micro_update:int):
	if(
		(_sm.update_counter != update_counter)	or
		(_nb_micro_update != micro_update_counter)
	):
		before_update_counter = update_counter
		before_micro_update_counter = micro_update_counter
		before_value = value
	
		update_counter = _sm.update_counter
		micro_update_counter = _nb_micro_update
		value = _value_fct.call(_sm, _event, _nb_micro_update)

func history_contain_one_tick():
	return (
		(
			(before_update_counter == update_counter) and 
			(before_micro_update_counter == micro_update_counter - 1)
		) or (
			(before_update_counter == update_counter-1) and 
			(micro_update_counter == 0)
		)
	)

## Returns true if the value obtained by the given function was false 
## before the last micro-update and is now true.
##
## _sm is the state machine
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func is_rising(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	_update(_sm, _event, _nb_micro_update)
	return ((not before_value) and value) and history_contain_one_tick()

## Returns true if the value obtained by the given functions was true before the
## last micro-update and is now false.
##
## _sm is the state machine
## _event_id is the event id of the candidate transition to be crossed.
## _nb_micro_update is the current number of micro_update minus 1.
func is_falling(_sm: SMStateMachine, _event:int, _nb_micro_update:int) -> bool:
	_update(_sm, _event, _nb_micro_update)
	return (before_value and (not value)) and history_contain_one_tick()
