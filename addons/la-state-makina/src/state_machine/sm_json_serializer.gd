## This class implements some tool to serialize and deserialize object in
## json.
##
## This class contains static functions that recursively serialize and 
## deserialize different types of objects.[br][br]
##
## In the State Machine, that class is used to serialize static and dynamic 
## metadata attached to the machine. For more details, you can consult the
## documentation of the [SMMachineState] class.[br][br]
##
## Supported types are : TYPE_NIL, TYPE_STRING, TYPE_STRING_NAME, TYPE_INT,
## TYPE_FLOAT, TYPE_BOOL, TYPE_DICTIONARY, TYPE_ARRAY, TYPE_VECTOR2,
## TYPE_VECTOR3, TYPE_VECTOR4.[br][br]
##
## Here an example of serialization and deserialization into a JSON string : 
## [codeblock]
## var data = {
##     "position" : Vector3(1,2,3),
##     "label" : "car",
##     "passenger_ids" : [4, 5, 1],
##     "tank capacity" : 20.0,
##     "metadata" : null
## }
## 
## var json_string : String = SMJsonSerializer.serialize(data)
##
## var errors : Array[String] = [] 
## var recovered_data = SMJsonSerializer.deserialize(json_string, errors)
## if len(errors) > 0 :
##     printerr("Some errors occur.")
##     printerr("Errors :")
##     printerr(errors)
## [/codeblock]
##
## Here's another example where we serialize an object into a JSON file :
## [codeblock]
## var data = {
##     "position" : Vector3(1,2,3),
##     "label" : "car",
##     "passenger_ids" : [4, 5, 1],
##     "tank capacity" : 20.0,
##     "metadata" : null
## }
##
## var file_path : String = "user://example.json"
## var success = SMJsonSerializer.serialize_in_file(data, file_path)
## if not success :
##     printerr("Some errors occurs.")
## [/codeblock]
##
## Here another example where we deserialize a json file into an object :
## [codeblock]
## var errors : Array[String] = []
## var recovered_data = SMJsonSerializer.deserialize_from_file(file_path, errors)
## if len(errors) > 0 :
##     printerr("Unable to deserialize the file.")
##     printerr("The following errors occurs : ")
##     printerr(errors)
## print(recovered_data)
## [/codeblock]
class_name SMJsonSerializer

## Converts a value into a Json String.[br][br]
##
## See [SMJsonSerializer] for some usage examples.[br][br]
##
## [param value] The object to serialize.[br] 
## [param indent] the indent to use in the Json.
static func serialize(value, indent="\t") -> String :
	var dict : Dictionary = dict_serialize(value)
	return JSON.stringify(dict, indent, true, true) 

## Converts a value into a dictionary that can be converted into a JSON string.
static func dict_serialize(value:Variant) -> Dictionary :
	if typeof(value) == Variant.Type.TYPE_NIL:
		return dict_serialize_nil()
	elif typeof(value) == Variant.Type.TYPE_STRING:
		return dict_serialize_string(value)
	elif typeof(value) == Variant.Type.TYPE_STRING_NAME:
		return dict_serialize_stringname(value)
	elif typeof(value) == Variant.Type.TYPE_INT:
		return dict_serialize_int(value)
	elif typeof(value) == Variant.Type.TYPE_FLOAT:
		return dict_serialize_float(value)
	elif typeof(value) == Variant.Type.TYPE_BOOL:
		return dict_serialize_bool(value)
	elif typeof(value) == Variant.Type.TYPE_DICTIONARY:
		var keys_are_string = true
		for key in value:
			if typeof(key) != TYPE_STRING:
				keys_are_string = false
				break
		if keys_are_string and not(("_type" in value) and ("_value" in value)):
			return dict_serialize_dict_with_string_keys(value)
		else:
			return dict_serialize_dict(value)
	elif typeof(value) == Variant.Type.TYPE_ARRAY:
		return dict_serialize_array(value)
	elif typeof(value) == Variant.Type.TYPE_VECTOR2:
		return dict_serialize_vector2(value)
	elif typeof(value) == Variant.Type.TYPE_VECTOR3:
		return dict_serialize_vector3(value)
	elif typeof(value) == Variant.Type.TYPE_VECTOR4:
		return dict_serialize_vector4(value)
	else:
		var text : String = (
			"Error in Json Serialisation : " + 
			"The type %s of the value %s is not implemented."%[
				typeof(value), value
			]
		)
		printerr(text)
		push_error(text)
		return {}

## Converts a String into a dictionary that can be converted into a JSON
## string.
static func dict_serialize_string(value:String) -> Dictionary :
	return {"_value":str(value), "_type":"String"}

## Converts the null reference into a dictionary that can be converted into a
## JSON string.
static func dict_serialize_nil() -> Dictionary :
	return {"_value":"", "_type":"nil"}

## Converts a StringName into a dictionary that can be converted into a JSON
## string.
static func dict_serialize_stringname(value:StringName) -> Dictionary :
	return {"_value":str(value), "_type":"StringName"}

## Converts a integer into a dictionary that can be converted into a JSON
## string.
static func dict_serialize_int(value:int) -> Dictionary :
	return {"_value":str(value), "_type":"int"}

## Converts a float into a dictionary that can be converted into a JSON string.
static func dict_serialize_float(value:float) -> Dictionary :
	var res : String = JSON.stringify(value,"", true, true)
	return {"_value":res, "_type":"float"}

## Converts a bool into a dictionary that can be converted into a JSON string.
static func dict_serialize_bool(value:bool) -> Dictionary :
	return {"_value":str(value), "_type":"bool"}

## Converts a dictionary where all keys are Sting (not StringName) into a 
## dictionary that can be converted into a JSON string.
##
## This function is used instead of
## [method SMJsonSerializer.dict_serialize_dict] to simplify the Json structure.
static func dict_serialize_dict_with_string_keys(dict:Dictionary):
	var res : Dictionary = {}
	for key in dict:
		res[key] = dict_serialize(dict[key])
	return res

## Converts a dictionary into a dictionary that can be converted into a JSON
## String.
static func dict_serialize_dict(dict:Dictionary) -> Dictionary :
	var res : Array = []
	for key in dict:
		res.append( [dict_serialize(key), dict_serialize(dict[key])] )
	return {"_value":res, "_type":"Dictionary"}

## Converts an array into a dictionary that can be converted into a JSON
## String.
static func dict_serialize_array(value:Array) -> Dictionary :
	var res : Array = []
	for v in value:
		res.append(dict_serialize(v))
	return {"_value" : res, "_type" : "Array"}

## Converts a Vector2 into a dictionary that can be converted into a JSON
## String.
static func dict_serialize_vector2(value:Vector2) -> Dictionary :
	var val = [
		JSON.stringify(value.x,"", true, true),
		JSON.stringify(value.y,"", true, true),
	]
	return {"_value" : val, "_type" : "Vector2"}

## Converts a Vector3 into a dictionary that can be converted into a JSON
## String.
static func dict_serialize_vector3(value:Vector3) -> Dictionary :
	var val = [
		JSON.stringify(value.x,"", true, true),
		JSON.stringify(value.y,"", true, true),
		JSON.stringify(value.z,"", true, true),
	]
	return {"_value" : val, "_type" : "Vector3"}

## Converts a Vector4 into a dictionary that can be converted into a JSON
## String.
static func dict_serialize_vector4(value:Vector4) -> Dictionary :
	var val = [
		JSON.stringify(value.x,"", true, true),
		JSON.stringify(value.y,"", true, true),
		JSON.stringify(value.z,"", true, true),
		JSON.stringify(value.w,"", true, true),
	]
	return {"_value" : val, "_type" : "Vector4"}

## Deserialize a JSON string into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## Notice, that if you have serialized null, that function returns null with no
## errors.[br][br]
##
## See [SMJsonSerializer] for some usage examples.[br][br]
##
## [param input_string] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize(input_string:String, errors:Array[String]) -> Variant :
	var dict = JSON.parse_string(input_string)
	return dict_deserialize(dict, errors)

## Deserialize a dictionary that can be converted into a Json String
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## Notice, that if you have serialized null, that function returns null with no
## errors.[br][br]
##
## [param dict] : the dictionary to deserialize.[br]
## [param errors] : an array to store the errors. 
static func dict_deserialize(dict:Dictionary, errors:Array[String]) -> Variant:
	var text : String
	if not (("_type" in dict) and ("_value" in dict) and (len(dict)==2)):
		if (not "_type" in dict) or (not "_value" in dict) :
			return deserialize_dict_with_string_keys(dict, errors)
		else:
			text = (
				"Invalid json format. " +
				"Need exactly 2 key : '_type' and '_value' key."
			)
			errors.append(text)
			return null
	var value = dict["_value"]
	var type = dict["_type"]
	if type == "nil":
		return null
	if type == "String":
		return deserialize_string(value, errors)
	elif type == "StringName":
		return deserialize_stringname(value, errors)
	elif type == "int":
		return deserialize_int(value, errors)
	elif type == "float":
		return deserialize_float(value, errors)
	elif type == "bool":
		return deserialize_bool(value, errors)
	elif type == "Dictionary":
		return deserialize_dictionary(value, errors)
	elif type == "Array":
		return deserialize_array(value, errors)
	elif type == "Vector2":
		return deserialize_vector2(value, errors)
	elif type == "Vector3":
		return deserialize_vector3(value, errors)
	elif type == "Vector4":
		return deserialize_vector4(value, errors)
	else:
		text = "Unknown type %s."%[type] 
		errors.append(text)
		return null

## Deserialize a string that contains a String into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_string(value:String, errors:Array[String]) -> String:
	return value

## Deserialize a string that contains a StringName into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_stringname(value:String, errors:Array[String]) -> StringName:
	return StringName(value)

## Deserialize a string that contains an integer into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_int(value:String, errors:Array[String]) -> int:
	return value.to_int()

## Deserialize a string that contains an float into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_float(value:String, errors:Array[String]) -> float:
	return JSON.parse_string(value)

## Deserialize a string that contains an boolean into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_bool(value:String, errors:Array[String]) -> bool:
	if value == "true" :
		return true
	elif value == "false":
		return false
	else:
		errors.append("Value %s is not a valide bool."%[value])
		return false

## Deserialize a string that contains a dictionary where all keys are String.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_dict_with_string_keys(
	dict:Dictionary, errors
) -> Dictionary :
	var res : Dictionary = {}
	for k in dict:
		res[k] = dict_deserialize(dict[k], errors)
	return res

## Deserialize a string that contains a dictionary into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_dictionary(array, errors:Array[String]) -> Dictionary:
	if typeof(array) != TYPE_ARRAY:
		errors.append("Value don't contains a dictionary.")
		return {}
	var res : Dictionary = {}
	for asso in array:
		var key = dict_deserialize(asso[0], errors)
		var value = dict_deserialize(asso[1], errors)
		res[key] = value
	return res

## Deserialize a string that contains an array into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_array(array, errors:Array[String]) -> Array:
	if typeof(array) != TYPE_ARRAY:
		errors.append("Value don't contains an array.")
		return []
	var res : Array = []
	for elem in array:
		res.append( dict_deserialize(elem, errors) )
	return res

## Deserialize a string that contains a Vector2 into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_vector2(value, errors:Array[String]) -> Vector2:
	if typeof(value) != TYPE_ARRAY or len(value) != 2:
		errors.append("Array of a Vector2 have to conains 2 elements.")
		return Vector2(0, 0)
	return Vector2(
		JSON.parse_string(value[0]), 
		JSON.parse_string(value[1])
	)

## Deserialize a string that contains a Vector3 into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_vector3(value, errors:Array[String]) -> Vector3:
	if typeof(value) != TYPE_ARRAY or len(value) != 3:
		errors.append("Array of a Vector3 have to conains 3 elements.")
		return Vector3(0, 0, 0)
	return Vector3(
		JSON.parse_string(value[0]), 
		JSON.parse_string(value[1]),
		JSON.parse_string(value[2])
	)

## Deserialize a string that contains a Vector4 into an object.[br][br]
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## [param value] : the string to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_vector4(value, errors:Array[String]) -> Vector4:
	if typeof(value) != TYPE_ARRAY or len(value) != 4:
		errors.append("Array of a Vector4 have to conains 4 elements.")
		return Vector4(0, 0, 0, 0)
	return Vector4(
		JSON.parse_string(value[0]), 
		JSON.parse_string(value[1]),
		JSON.parse_string(value[2]),
		JSON.parse_string(value[3])
	)

## Serialize an object and store it in a JSON file.[br][br]
##
## See [SMJsonSerializer] for some usage examples.[br][br]
##
## [param value]: the object to serialize.[br] 
## [param file_path]: the path to store the serialized object.[br]
## [param indent]: the indentation to use for the json format.
static func serialize_in_file(value, file_path:String, indent="\t") -> bool :
	var file = FileAccess.open(file_path, FileAccess.WRITE)
	if file == null:
		return false
	file.store_string(serialize(value, indent))
	return true

## Returns the object obtained by deserializing a JSON file.
##
## When deserialize fails, it returns null and store in the array 
## [param errors] all the obtained errors.[br][br]
##
## See [SMJsonSerializer] for some usage examples.[br][br]
##
## [param file_path]: the file path to deserialize.[br]
## [param errors] : an array to store the errors. 
static func deserialize_from_file(
	file_path:String, errors:Array[String]
) -> Variant :
	if( !FileAccess.file_exists(file_path) ):
		var text = "The file %s doesn't exists."%file_path
		errors.append(text)
		return null
	var file = FileAccess.open(file_path, FileAccess.READ)
	if file == null:
		var text = "Unable to open the file %s for reading."%file_path
		errors.append(text)
		return null
	var content = file.get_as_text()
	return deserialize(content, errors)
