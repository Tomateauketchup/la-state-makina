class_name SMDebugTools

@warning_ignore("shadowed_global_identifier")
static func print(obj : Variant=""):
	var text : String = str(obj)
	var frame = get_stack()
	if len(frame) < 2:
		print(text + " - line ? : no debug server or in thread mode (see get_stack())")
		return
	var line = "line: " + str(frame[1]["line"])
	var source = str(frame[1]["source"]) #.get_file())
	print("%s - %s, %s"% [text, line, source])
